﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;
using Microsoft.AspNet.SignalR.Hubs;
using ProAlert.Andon.Service.Interfaces;
using ProAlert.Andon.Service.Models;
using ProAlert.Andon.Service.ViewModels;
using Vistech.Andon.Web.Tools;

namespace Vistech.Andon.Web
{

    public class OeeSummary : IOeeSummary
    {
        private readonly Timer _timer;
        private readonly object _updateOeeSummaryLock = new object();
        private volatile bool _updatingOeeSummary = false;
        private DateTime _start;

        //public DateTime Start
        //{
        //    get { return _start; }
        //    set { _start = value; }
        //}

        public OeeSummary(IHubConnectionContext<dynamic> clients)
        {
            Clients = clients;
            UpdateAll();
            _timer = new Timer { AutoReset = false };
            _timer.Elapsed += (TimeElapsed);
            _timer.Start();
        }
        static double GetInterval()
        {
            var now = DateTime.Now;
            return ((60 - now.Second) * 1000 - now.Millisecond) % 6;
        }
        private void TimeElapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            var now = DateTime.Now;
            // check every ten seconds
            if (!_updatingOeeSummary)
                UpdateAll();
            _timer.Interval = 10000; //GetInterval();
            _timer.Start();
        }
        private IHubConnectionContext<dynamic> Clients
        {
            get;
            set;
        }

        public Task UpdateAll()
        {
            lock (_updateOeeSummaryLock)
            {
                if (!_updatingOeeSummary)
                {
                    try
                    {
                        _updatingOeeSummary = true;
                        using (var context = new ProAlertContext())
                        {
                            //context.Configuration.LazyLoadingEnabled = false;
                            //context.Configuration.ProxyCreationEnabled = false;
                            using (var repo = new EntityFrameworkRepository<ProAlertContext>(context))
                            {
                                var gs = repo.GetFirst<GlobalSetting>();
                                _start = gs.OeeSumRollEight ? DateTime.UtcNow.AddHours(-8) : gs.OeeSummaryStartDt;
                                var obj = new TlRpt();
                                var tlList = obj.GetTls(repo.Get<WorkCenter>().ToList(), _start, DateTime.UtcNow);

                                foreach (var tl in tlList.ToList())
                                {
                                    tl.CacheID = tl.TL.Id.ToString();

                                    var sd = tl.OeeSummaryDetail;
                                    var target = tlList.Average(x => x.Target);
                                    var actual = tlList.Average(x => x.OEE);
                                    sd.TargetOeeAvg = "ProAlert Target OEE:  " + target.ToString("P1");
                                    sd.TotalOeeAvg = "<b>Total OEE: " + actual.ToString("P1") + "</b>";
                                    sd.Ocolor = actual < target ? "red" : "white";
                                    BroadcastUpdate(sd);
                                }
                            }
                            //context.Configuration.LazyLoadingEnabled = true;
                            //context.Configuration.ProxyCreationEnabled = true;
                        }
                    }
                    catch (Exception e)
                    {
                        using (var context = new ProAlertContext())
                        {
                            using (var repo = new EntityFrameworkRepository<ProAlertContext>(context))
                            {
                                repo.InsertError(new AppError
                                {
                                    ErrorDT = DateTime.UtcNow,
                                    ErrorMsg = e.Message,
                                    Source = "oeeSummaryHub"
                                });
                            }
                        }
                    }
                    finally
                    {
                        _updatingOeeSummary = false;

                    }

                }
            }
            return null;
        }
        public Task InitiateUpdate(int id)
        {
            lock (_updateOeeSummaryLock)
            {
                if (!_updatingOeeSummary)
                {
                    try
                    {
                        _updatingOeeSummary = true;
                        using (var context = new ProAlertContext())
                        {
                            //context.Configuration.ProxyCreationEnabled = false;
                            using (var repo = new EntityFrameworkRepository<ProAlertContext>(context))
                            {
                                var gs = repo.GetFirst<GlobalSetting>();
                                _start = gs.OeeSumRollEight ? DateTime.UtcNow.AddHours(-8) : gs.OeeSummaryStartDt;

                                var obj = new TlRpt();
                                var tl = obj.GetTlByWc(id, _start, DateTime.UtcNow);
                                tl.CacheID = tl.TL.Id.ToString();

                                var sd = tl.OeeSummaryDetail;
                                sd.TargetOeeAvg = "";
                                sd.TotalOeeAvg = "";
                                sd.Ocolor = "";
                                BroadcastUpdate(sd);
                            }
                            //context.Configuration.ProxyCreationEnabled = true;
                        }
                    }
                    catch (Exception e)
                    {
                        using (var context = new ProAlertContext())
                        {
                            using (var repo = new EntityFrameworkRepository<ProAlertContext>(context))
                            {
                                repo.InsertError(new AppError
                                {
                                    ErrorDT = DateTime.UtcNow,
                                    ErrorMsg = e.Message,
                                    Source = "oeeSummaryHub"
                                });
                            }
                        }
                    }
                    finally
                    {
                        _updatingOeeSummary = false;

                    }

                }
            }
            return null;
        }
        private void BroadcastUpdate(OEESummaryDetail sd)
        {
            Clients.All.Update(sd);
        }
    }
}