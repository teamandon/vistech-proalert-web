﻿$(function () {
    var proxy = $.connection.oeeSummary,
        $totOee = $("#TotalOee"),
        $targetOee = $("#TargetOee");

    function buildRow(data) {
        return "<tr id='" +
            data.CacheId +
            "'>" +
            "<td style='visibility: hidden; width: 0;'><input type='hidden' value=" + data.Current + "/></td>" +
            "<td title='" +
            data.TimeFrame +
            "' class=" +
            data.WcStatusColor +
            " style='padding-left: 3px;'>" +
            data.WcProductCyclesPerMin +
            "</td>" +
            "<td class=" +
            data.WcStatusColor +
            " style='text-align: center'>" +
            data.PlannedVsOperational +
            "</td>" +
            "<td class=" +
            data.WcStatusColor +
            " style='text-align: center'>" +
            data.Down +
            "</td>" +
            "<td class=" +
            data.WcStatusColor +
            " style='text-align: center'>" +
            data.LastDown +
            "</td>" +
            "<td class=" +
            data.WcStatusColor +
            " style='text-align: center' title='" +
            data.ScrapTitle +
            "'>" +
            data.Scrap +
            "</td>" +
            "<td class=" +
            data.WcStatusColor +
            " style='text-align: center'>" +
            data.TargetVsActual +
            "</td>" +
            "<td class=" +
            data.AvailColor +
            " style='text-align: center'>" +
            data.Availability +
            "</td>" +
            "<td class=" +
            data.PerformColor +
            " style='text-align: center'>" +
            data.Performance +
            "</td>" +
            "<td class=" +
            data.QualColor +
            " style='text-align: center'>" +
            data.Quality +
            "</td>" +
            "<td class=" +
            data.OeeColor +
            " style='text-align: center'>" +
            data.Oee +
            "</td></tr>";
    }

    function sortByCurrentOee(row1, row2) {
        var v1, v2, r;
        v1 = $(row1).find("input").val();
        v2 = $(row2).find("input").val();
        // current in descending order.
        if (v1 < v2) {
            r = 1;
        } else if (v1 > v2) {
            r = -1;
        } else {
            r = 0;
        }
        if (r === 0) {
            // current is the same, compare OEE %
            v1 = $(row1).find("td:eq(10)").text().replace(' %', '');
            v2 = $(row2).find("td:eq(10)").text().replace(' %', '');
            v1 = parseFloat(v1);
            v2 = parseFloat(v2);
            if (v1 < v2) {
                r = -1;
            } else if (v1 > v2) {
                r = 1;
            } else {
                r = 0;
            }
        }
        if (r === 0) {
            // Current & OEE % same, compare WcProduct name
            v1 = $(row1).find("td:eq(1)").text();
            v2 = $(row2).find("td:eq(1)").text();
            if (v1 < v2) {
                r = -1;
            } else if (v1 > v2) {
                r = 1;
            } else {
                r = 0;
            }
        }
        return r;
    }

    function sortCurrent() {
        var rows = $("#oeeCurrent tbody tr").detach().get();
        rows.sort(sortByCurrentOee);
        $("#oeeCurrent tbody").append(rows);
    }

    function formatOeeSum(data) {
        return $.extend(data,
        {
            CacheId: data.CacheId,
            TimeFrame: data.TimeFrame,
            WcProductCyclesPerMin: data.WcProductCyclesPerMin,
            PlannedVsOperational: data.PlannedVsOperational,
            Down: data.Down,
            LastDown: data.LastDown,
            ScrapTitle: data.ScrapTitle,
            Scrap: data.Scrap,
            TargetVsActual: data.TargetVsActual,
            Availability: data.Availability,
            Performance: data.Performance,
            Quality: data.Quality,
            Oee: data.Oee,
            AvailColor: data.AvailColor,
            PerformColor: data.PerformColor,
            QualColor: data.QualColor,
            OeeColor: data.OeeColor,
            WcStatusColor: data.WcStatusColor,
            Ocolor: data.Ocolor,
            TotalOeeAvg: data.TotalOeeAvg,
            TargetOeeAvg: data.TargetOeeAvg,
            Current: data.Current
        });
    }

    proxy.client.update = function (data) {
        var updateRow = formatOeeSum(data);
        // delete the row being updated.
        $("#" + updateRow.CacheId).remove();
        var r = buildRow(updateRow);
        // add the updated row
        //var row = $("#oeeSummary tbody tr").find(data.CacheId);
        $("#oeeCurrent tbody").append(r);
        // sort table rows
        sortCurrent();
        if (updateRow.TargetOeeAvg !== "") {
            $targetOee.html(updateRow.TargetOeeAvg);
        }
        if (updateRow.Ocolor !== "") {
            $totOee.css("color", updateRow.Ocolor);
        }
        if (updateRow.TotalOeeAvg !== "") {
            $totOee.html(updateRow.TotalOeeAvg);
        }
    }
    // Start the connection
    $.connection.hub.start().done();
});