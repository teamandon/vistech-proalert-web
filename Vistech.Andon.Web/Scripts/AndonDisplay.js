﻿// A simple templating method for replacing placeholders enclosed in curly braces.
//if (!String.prototype.supplant) {
//    String.prototype.supplant = function (o) {
//        return this.replace(/{([^{}]*)}/g,
//            function (a, b) {
//                var r = o[b];
//                return typeof r === 'string' || typeof r === 'number' ? r : a;
//            }
//        );
//    };
//}

$(function () {
    var msgset = $.connection.andonDisplay,
    $msgTable = $('#adTable'),
    $maint = $msgTable.find('#Maint'),
    $qc = $msgTable.find('#QC'),
    $mat = $msgTable.find('#Mat'),
    $time = $msgTable.find('#Time'),
    $advis = $msgTable.find('#Advis');
    var audioElement = document.createElement('audio');
    audioElement.setAttribute('src', '../sounds/alarm1.wav');

    function checkDt() {
        var dt = false;
        $('.lblDT').each(function () {
            dt = true;
        });
        $('.lblNonDT').each(function () {
            dt = true;
        });
        if (dt === true) {
            audioElement.play();
        }
    }

    function getDt() {
        var today = new Date();
        var h = today.getHours();
        var m = today.getMinutes();
        var s = today.getSeconds();
        h = checkTime(h);
        m = checkTime(m);
        s = checkTime(s);
        $time.html("<b>" + today.toLocaleDateString() + " " + h + ":" + m + ":" + s + "</b>");
        var t = setTimeout(function () { getDt(); }, 1000);
    }
    function checkTime(i) {
        if (i < 10) {
            i = "0" + i;
        };  // add zero in front of numbers < 10
        return i;
    }


    function formatMsg(data) {
        return $.extend(data,
        {
            Maintenance: data.Maintenance,
            Supervisor: data.Supervisor,
            Materials: data.Materials
        });
    }

    function formatAdMsg(data) {
        return $.extend(data,
        {
            Advisory: data.Advisory,
            Image: data.Image

        });
    }

    function init() {
        msgset.server.getMsgSet().done(function (data) {
            var displayMsg = formatMsg(data);
            $maint.html(displayMsg.Maintenance);
            $qc.html(displayMsg.Supervisor);
            $mat.html(displayMsg.Materials);
        });

        msgset.server.getAdSet().done(function (data) {
            var displayAdMsg = formatAdMsg(data);
            $advis.html(displayAdMsg.Advisory);
            if (displayAdMsg.Advisory.length === 0) {
                $("#time").show();
            } else {
                $("#time").hide();
            }
        });

        getDt();
    }

    msgset.client.updateAdvisory = function (data) {
        var displayMsg2 = formatMsg(data);
        $advis.html(displayMsg2.Advisory);
        if (displayMsg2.Advisory.length === 0) {
            $("#time").show();
        } else {
            $("#time").hide();
        }
    }

    msgset.client.updateAndonDisplay = function (data) {
        var displayMsg = formatMsg(data);
        $maint.html(displayMsg.Maintenance);
        $qc.html(displayMsg.Supervisor);
        $mat.html(displayMsg.Materials);
        if (document.title !== "Andon Display Shop floor") {
            checkDt();
        }
    }
    // Start the connection
    $.connection.hub.start().done(init);

    $.connection.hub.disconnected(function () {
        setTimeout(function () {
            $.connection.hub.start();
        }, 5000); // Restart connection after 5 seconds.
    });
});