﻿
    $('#AddPosition').on('click', function () {
        $("#AddPositionForm").dialog("open");
        return false;
    });

$("#AddPositionForm").dialog({
    autoOpen: false,
    width: 400,
    resizable: false,
    title: 'Add Position Form',
    modal: true,
    open: function () {
        $(this).load('@Url.Action("PositionCreate")', function() {
            $("#Title").focus();
        });
    },
    buttons: {
        "Add Position": function () {
            addPositionInfo();
        },
        Cancel: function () {
            $(this).dialog("close");
        }
    }
});

function addPositionInfo() {
    var dlg = $("#AddPositionForm");
    $.ajax({
        url: '@Url.Action("PositionCreate", "Employee")',
        type: "POST",
        data: $("#target").serialize(),
        success: function (data) {
            if (data) {
                var positionDdl = $("#Employee_PositionID");
                positionDdl.empty();
                $.each(data.sl, function (i, obj) {
                    positionDdl.append($('<option/>').val(obj.Value).text(obj.Text)).change();
                });
                positionDdl.val(data.idx);
                dlg.dialog("close");
                $("#ajaxResult").hide().html('Record saved').fadeIn(300, function () {
                    var e = this;
                    setTimeout(function () { $(e).fadeOut(400); }, 2500);
                });
            }
        },
        error: function(xhr) {
            if (xhr.status == 400)
                dlg.html(xhr.responseText, xhr.status);     /* display validation errors in edit dialog */
            else
                displayError(xhr.responseText, xhr.status); /* display other errors in separate dialog */

        }
    });

}


function displayError(message, status) {
    var $dialog = $(message);
    $dialog
        .dialog({
            modal: true,
            title: status + ' Error',
            buttons: {
                Ok: function () {
                    $(this).dialog("close");
                }
            }
        });
    return false;
};
