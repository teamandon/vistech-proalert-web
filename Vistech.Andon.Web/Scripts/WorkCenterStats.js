﻿$(function () {
    var msgset = $.connection.workcenterOee,
    $statTable = $('#statsTable'),
    $lastDt = $statTable.find('#lastDt'),
    $totalDt = $statTable.find('#totalDt'),
    $scrap = $statTable.find('#scrap'),
    $rework = $statTable.find('#rework'),
    $pieceCnt = $statTable.find('#pieceCnt'),
    $cycles = $statTable.find("#cycles"),
    $minutesPrePiece = $statTable.find("#minutesPerPiece"),
    $operationTimeInMin = $statTable.find("#operationTimeInMin");

    function formatWcStats(data) {
        return $.extend(data,
        {
            LastDTReason: data.LastDTReason,
            TotalDT: data.TotalDT,
            Scrap: data.Scrap,
            Rework: data.Rework,
            PieceCnt: data.PieceCnt,
            Cycles: data.Cycles,
            MinutesPerPiece: data.MinutesPerPiece,
            OperationTimeInMin: data.OperationTimeInMin
        });
    }

    function init() {

    }


    msgset.client.updateWcStats = function (data) {
        var displayWcStats = formatWcStats(data);
        $lastDt.html(displayWcStats.LastDTReason);
        $totalDt.html(displayWcStats.TotalDT);
        $scrap.html(displayWcStats.Scrap);
        $rework.html(displayWcStats.Rework);
        $pieceCnt.html(displayWcStats.PieceCnt);
        $cycles.html(displayWcStats.Cycles);
        $minutesPrePiece.html(displayWcStats.MinutesPerPiece);
        $operationTimeInMin.html(displayWcStats.OperationTimeInMin);
    }
    // Start the connection
    $.connection.hub.start().done(init);
});