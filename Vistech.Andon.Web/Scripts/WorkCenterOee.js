﻿var oeeHubProxy;

$(function () {
    var connection = $.hubConnection();
    oeeHubProxy = connection.createHubProxy('workcenterOee');
    var $oeeTable = $('#tOEEHeader'),
    $p = $oeeTable.find('#P'),
    $a = $oeeTable.find('#A'),
    $q = $oeeTable.find('#Q'),
    $oee = $oeeTable.find('#OEE'),
    $ta = $oeeTable.find('#TA');

    var $statTable = $('#statsTable'),
    $lastDt = $statTable.find('#lastDt'),
    $totalDt = $statTable.find('#totalDt'),
    $scrap = $statTable.find('#scrap'),
    $rework = $statTable.find('#rework'),
    $pieceCnt = $statTable.find('#pieceCnt'),
    $cycles = $statTable.find("#cycles"),
    $minutesPrePiece = $statTable.find("#minutesPerPiece"),
    $operationTimeInMin = $statTable.find("#operationTimeInMin");

    function formatOeeHeader(data) {
        return $.extend(data,
        {
            Performance: data.Performance,
            Availability: data.Availability,
            Quality: data.Quality,
            OEE: data.OEE,
            Target: data.Target
        });
    }
    function formatWcStats(data) {
        return $.extend(data,
        {
            LastDTReason: data.LastDTReason,
            TotalDTInMin: data.TotalDTInMin,
            Scrap: data.Scrap,
            Rework: data.Rework,
            PieceCnt: data.PieceCnt,
            Cycles: data.Cycles,
            MinutesPerPiece: data.MinutesPerPiece,
            OperationTimeInMin: data.OperationTimeInMin
        });
    }


    function init() {
        //msgset.server.getMsgSet().done(function (data) {
        //    var displayMsg = formatMsg(data);
        //    $maint.html(displayMsg.Maintenance);
        //    $qc.html(displayMsg.Supervisor);
        //    $mat.html(displayMsg.Materials);
        //});

        //msgset.server.getAdSet().done(function (data) {
        //    var displayAdMsg = formatAdMsg(data);
        //    $advis.html(displayAdMsg.Advisory);
        //    if (displayAdMsg.Advisory.length === 0) {
        //        $("#time").show();
        //    } else {
        //        $("#time").hide();
        //    }
        //});

        //getDt();
    }

    oeeHubProxy.on('updateOeeHeader',
        function(data) {
            var displayOeeHeader = formatOeeHeader(data);
            $p.html("P=" + displayOeeHeader.Performance);
            $a.html("A=" + displayOeeHeader.Availability);
            $q.html("Q=" + displayOeeHeader.Quality);
            $oee.html("OEE=" + displayOeeHeader.OEE);
            $ta.html("T/A=" + displayOeeHeader.Target + "@" + displayOeeHeader.OEE);
        });

    //oeeConn.client.updateOeeHeader = function (data) {
    //    var displayOeeHeader = formatOeeHeader(data);
    //    $p.html("P=" + displayOeeHeader.Performance);
    //    $a.html("A=" + displayOeeHeader.Availability);
    //    $q.html("Q=" + displayOeeHeader.Quality);
    //    $oee.html("OEE=" + displayOeeHeader.OEE);
    //    $ta.html("T/A=" + displayOeeHeader.Target + "@" + displayOeeHeader.OEE);
    //}

    oeeHubProxy.on('updateWcStats',
        function(data) {
            var displayWcStats = formatWcStats(data);
            $lastDt.html(displayWcStats.LastDTReason);
            $totalDt.html(displayWcStats.TotalDTInMin);
            $scrap.html(displayWcStats.Scrap);
            $rework.html(displayWcStats.Rework);
            $pieceCnt.html(displayWcStats.PieceCnt);
            $cycles.html(displayWcStats.Cycles);
            $minutesPrePiece.html(displayWcStats.MinutesPerPiece);
            $operationTimeInMin.html(displayWcStats.OperationTimeInMin);
        });

    //oeeConn.client.updateWcStats = function (data) {
    //    var displayWcStats = formatWcStats(data);
    //    $lastDt.html(displayWcStats.LastDTReason);
    //    $totalDt.html(displayWcStats.TotalDTInMin);
    //    $scrap.html(displayWcStats.Scrap);
    //    $rework.html(displayWcStats.Rework);
    //    $pieceCnt.html(displayWcStats.PieceCnt);
    //    $cycles.html(displayWcStats.Cycles);
    //    $minutesPrePiece.html(displayWcStats.MinutesPerPiece);
    //    $operationTimeInMin.html(displayWcStats.OperationTimeInMin);
    //}
    // Start the connection
    //$.connection.hub.start().done(init);
});