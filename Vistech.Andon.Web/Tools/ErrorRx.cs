﻿using System;
using ProAlert.Andon.Service.Interfaces;
using ProAlert.Andon.Service.Models;

namespace Vistech.Andon.Web.Tools
{

    public class ErrorRx
    {
        private IRepository _repo;

        public IRepository Repo
        {
            get { return _repo; }
            set { _repo = value; }
        }

        public void RecordError(string ex, string controller, string action, int userId)
        {
            _repo.InsertError(new AppError
            {
                ErrorMsg = ex,
                ErrorDT = DateTime.UtcNow,
                UserID = userId,
                Source = controller + " : " + action
    
            });
        }
    }
}