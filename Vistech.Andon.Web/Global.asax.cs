﻿using System;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
//using Autofac;
//using Autofac.Integration.Mvc;
//using Vistech.Andon.Web.Modules;

namespace Vistech.Andon.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);


            ////Autofac Configuration
            //var builder = new Autofac.ContainerBuilder();

            //builder.RegisterControllers(typeof(MvcApplication).Assembly).PropertiesAutowired();


            //builder.RegisterModule(new ServiceModule());
            //builder.RegisterModule(new EFModule());

            //var container = builder.Build();

            //DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
        /// <summary>
        /// Handles the BeginRequest event of the application control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            if (Request.IsSecureConnection)
            {
                Response.AddHeader("Strict-Transport-Security", "max-age=31536000; includeSubDomains");
            }
        }
    }
}
