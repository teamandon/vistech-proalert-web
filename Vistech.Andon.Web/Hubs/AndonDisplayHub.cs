﻿using System.Threading.Tasks;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using ProAlert.Andon.Service.Models;

namespace Vistech.Andon.Web.Hubs
{
    [HubName("andonDisplay")]
    public class AndonDisplayHub : Hub
    {
        private readonly IAndonDisplay _andonDisplay;

        public AndonDisplayHub(IAndonDisplay andonDisplay)
        {
            _andonDisplay = andonDisplay;
        }

        public void CustomMsg(string advisory)
        {
            _andonDisplay.CustomMsg(advisory);
        }
        public async Task InitiateCall(int id)
        {
            using (var context = new ProAlertContext())
            {
                await _andonDisplay.InitiateCall(context, id);
            }
        }

        public AndonDisplayMsgSet GetMsgSet()
        {
            return _andonDisplay.GetMsgSet();
        }

        public AdvisoryMsgSet GetAdSet()
        {
            return _andonDisplay.GetAdSet();
        }
    }
}