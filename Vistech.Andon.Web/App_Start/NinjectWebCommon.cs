using Vistech.Andon.Web.Hubs;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(Vistech.Andon.Web.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(Vistech.Andon.Web.App_Start.NinjectWebCommon), "Stop")]

namespace Vistech.Andon.Web.App_Start
{
    using System;
    using System.Web;
    using Microsoft.Web.Infrastructure.DynamicModuleHelper;
    using Ninject;
    using Ninject.Web.Common;
    using ProAlert.Andon.Service.Interfaces;
    using Microsoft.AspNet.SignalR;
    using System.Collections.Generic;
    using System.Linq;
    using ProAlert.Andon.Service.Models;
    using Microsoft.AspNet.SignalR.Hubs;
    using Microsoft.AspNet.SignalR.Infrastructure;
    using System.Web.Http;

    internal class NinjectSignalRDependencyResolver : DefaultDependencyResolver
    {
        private readonly IKernel _kernel;
        public NinjectSignalRDependencyResolver(IKernel kernel)
        {
            _kernel = kernel;
        }

        public override object GetService(Type serviceType)
        {
            return _kernel.TryGet(serviceType) ?? base.GetService(serviceType);
        }

        public override IEnumerable<object> GetServices(Type serviceType)
        {
            return _kernel.GetAll(serviceType).Concat(base.GetServices(serviceType));
        }
    }
    public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();
                GlobalHost.DependencyResolver = new NinjectSignalRDependencyResolver(kernel);

                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<ProAlertContext>().ToSelf().InRequestScope();
            kernel.Bind<IRepository>().To<EntityFrameworkRepository<ProAlertContext>>().InRequestScope();
            kernel.Bind<IReadOnlyRepository>().To<EfReadOnlyRepository.EntityFrameworkReadOnlyRepository<ProAlertContext>>().InRequestScope();
            //GlobalConfiguration.Configuration.DependencyResolver = kernel.Get<System.Web.Http.Dependencies.IDependencyResolver>();

            kernel.Bind<IAndonDisplay>().To<AndonDisplay>().InSingletonScope(); // Make it a singleton object
            kernel.Bind(typeof(IHubConnectionContext<dynamic>))
                .ToMethod(
                    context =>
                        GlobalHost.DependencyResolver.Resolve<IConnectionManager>()
                            .GetHubContext<AndonDisplayHub>()
                            .Clients)
                .WhenInjectedInto<IAndonDisplay>();

            kernel.Bind<IWorkCenterOee>().To<WorkCenterOee>().InSingletonScope(); // Make it a singleton object
            kernel.Bind(typeof(IHubConnectionContext<dynamic>))
                .ToMethod(
                    context =>
                        GlobalHost.DependencyResolver.Resolve<IConnectionManager>()
                            .GetHubContext<WorkCenterOeeHub>()
                            .Clients)
                .WhenInjectedInto<IWorkCenterOee>();

            kernel.Bind<IOeeSummary>().To<OeeSummary>().InSingletonScope(); // Make it a singleton object
            kernel.Bind(typeof(IHubConnectionContext<dynamic>))
                .ToMethod(
                    context =>
                        GlobalHost.DependencyResolver.Resolve<IConnectionManager>()
                            .GetHubContext<OeeSummaryHub>()
                            .Clients)
                .WhenInjectedInto<IOeeSummary>();
        }

    }
}
