﻿using System;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using ProAlert.Andon.Service.Interfaces;
using ProAlert.Andon.Service.Models;


namespace Vistech.Andon.Web.Controllers
{
    //[InitializeSimpleMembership]
    [Authorize(Roles = "Admin")]
    public class CallController : Controller
    {
        #region < Members >
        private readonly IRepository _repo;
        #endregion

        //
        // GET: /Call/

        public CallController(IRepository repo)
        {
            _repo = repo;
        }

        public ActionResult Index()
        {
            var calls = _repo.Get<Call>(includeProperties: "AudioFile");
            return View(calls.ToList());
        }

        //
        // GET: /Call/Details/5

        public ActionResult Details(int id = 0)
        {
            var call = _repo.GetById<Call>(id);
            if (call == null)
            {
                return HttpNotFound();
            }
            return View(call);
        }

        //
        // GET: /Call/Create

        public ActionResult Create()
        {
            PopulateAudioFileDD();
            return View();
        }

        private void PopulateAudioFileDD(object selectedAudioFile = null)
        {
            var audiofileQuery = from af in _repo.Get<AudioFile>() 
                orderby af.Name
                select new {af.Id, af.Name };

            ViewBag.AudioFileID = new SelectList(audiofileQuery, "Id", "Name", selectedAudioFile);
        }
        //
        // POST: /Call/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="Name, Type, DT, Description, AudioFileID, RepeatCnt")] Call call)
        {
            if (ModelState.IsValid)
            {
                _repo.Create(call, User.Identity.Name);
                _repo.Save();
                return RedirectToAction("Index");
            }

            PopulateAudioFileDD(call.AudioFileID);
            return View(call);
        }

        //
        // GET: /Call/Edit/5

        public ActionResult Edit(int id = 0)
        {
            var call = _repo.GetById<Call>(id);
            if (call == null) return HttpNotFound();
            PopulateAudioFileDD(call.AudioFileID);
            return View(call);
        }

        //
        // POST: /Call/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id, Name, Type, DT, Description, AudioFileID")] Call call)
        {

            if (ModelState.IsValid)
            {
                var callToUpdate = _repo.GetById<Call>(call.Id);
                callToUpdate.AudioFileID = call.AudioFileID;
                callToUpdate.Name = call.Name;
                callToUpdate.DT = call.DT;
                callToUpdate.Description = call.Description;
                callToUpdate.Type = call.Type;

                _repo.Update(callToUpdate, User.Identity.Name);
                _repo.Save();
                return RedirectToAction("Index");
            }
            PopulateAudioFileDD(call.AudioFileID);
            return View(call);
        }

        //
        // GET: /Call/Delete/5

        public ActionResult Delete(int id = 0)
        {
            var call = _repo.GetById<Call>(id);
            ViewBag.Msg = _repo.Get<CallLog>(x => x.CallID == id).Any() ? "Cannot delete." : "Are you sure you want to delete?";
            return call == null ? (ActionResult) HttpNotFound() : View(call);
        }

        //
        // POST: /Call/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                _repo.Delete<Call>(id);
                _repo.Save();
            }
            catch (Exception ex)
            {
                if (!ex.Message.StartsWith("Violation of UNIQUE KEY constraint")) return RedirectToAction("Index");
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                ModelState.AddModelError("Name", "You cannot delete related data.");
            }
            return RedirectToAction("Index");
        }

    }
}