﻿using System;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using ProAlert.Andon.Service.Interfaces;
using ProAlert.Andon.Service.Models;


namespace Vistech.Andon.Web.Controllers
{
    //[InitializeSimpleMembership]
    [Authorize(Roles = "Admin")]
    public class CallLogController : Controller
    {
        #region < Members >
        private readonly IRepository _repo;
        #endregion

        //
        // GET: /CallLog/
        public CallLogController(IRepository repo)
        {
            _repo = repo;
        }
        public ActionResult Index()
        {
            var calllogs = _repo.GetAll<CallLog>(includeProperties: "Call, Employee, WorkCenter, Issue");
            return View(calllogs.ToList());
        }

        //
        // GET: /CallLog/Details/5

        public ActionResult Details(int id = 0)
        {
            var calllog = _repo.GetById<CallLog>(id);
            return calllog == null ? (ActionResult) HttpNotFound() : View(calllog);
        }

        //
        // GET: /CallLog/Create

        public ActionResult Create()
        {
            ViewBag.CallID = new SelectList(_repo.GetAll<CallLog>(), "Id", "Name");
            ViewBag.EmployeeID = new SelectList(_repo.GetAll<Employee>(), "Id", "LogIn");
            ViewBag.WorkCenterId = new SelectList(_repo.GetAll<WorkCenter>(), "Id", "Name");
            ViewBag.IssueID = new SelectList(_repo.GetAll<Issue>(), "Id", "Name");
            return View();
        }

        private void PopulateCallDD(object selectedCall = null)
        {
            var callQuery = from c in _repo.GetAll<Call>()
                orderby c.Name
                select c;
            ViewBag.CallID = new SelectList(callQuery, "Id", "Name", selectedCall);
        }

        private void PopulateIssueDD(object selectedIssue = null)
        {
            var issueQuery = from i in _repo.GetAll<Issue>()
                orderby i.Name
                select i;
            ViewBag.IssueID = new SelectList(issueQuery, "Id", "Name", selectedIssue);
        }

        //
        // POST: /CallLog/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CallLog calllog)
        {
            if (ModelState.IsValid)
            {
                _repo.Create(calllog, User.Identity.Name);
                _repo.Save();
                return RedirectToAction("Index");
            }

            ViewBag.CallID = new SelectList(_repo.GetAll<Call>(), "Id", "Name", calllog.CallID);
            ViewBag.EmployeeID = new SelectList(_repo.GetAll<Employee>(), "Id", "LogIn", calllog.EmployeeID);
            ViewBag.WorkCenterId = new SelectList(_repo.GetAll<WorkCenter>(), "Id", "Name", calllog.WorkCenterId);
            ViewBag.IssueID = new SelectList(_repo.GetAll<Issue>(), "Id", "Name", calllog.IssueID);
            return View(calllog);
        }

        //
        // GET: /CallLog/Edit/5

        public ActionResult Edit(int id = 0)
        {
            var calllog = _repo.GetById<CallLog>(id);
            if (calllog != null)
            {
                ViewBag.CallID = new SelectList(_repo.GetAll<Call>(), "Id", "Name", calllog.CallID);
                ViewBag.EmployeeID = new SelectList(_repo.GetAll<Employee>(), "Id", "LogIn", calllog.EmployeeID);
                ViewBag.WorkCenterId = new SelectList(_repo.GetAll<WorkCenter>(), "Id", "Name", calllog.WorkCenterId);
                ViewBag.IssueID = new SelectList(_repo.GetAll<Issue>(), "Id", "Name", calllog.IssueID);
                return View(calllog);
            }
            return HttpNotFound();
        }

        //
        // POST: /CallLog/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CallLog calllog)
        {
            if (ModelState.IsValid)
            {
                _repo.Update(calllog, User.Identity.Name);
                _repo.Save();
                return RedirectToAction("Index");
            }
            ViewBag.CallID = new SelectList(_repo.GetAll<Call>(), "Id", "Name", calllog.CallID);
            ViewBag.EmployeeID = new SelectList(_repo.GetAll<Employee>(), "Id", "LogIn", calllog.EmployeeID);
            ViewBag.WorkCenterId = new SelectList(_repo.GetAll<WorkCenter>(), "Id", "Name", calllog.WorkCenterId);
            ViewBag.IssueID = new SelectList(_repo.GetAll<Issue>(), "Id", "Name", calllog.IssueID);
            return View(calllog);
        }

        //
        // GET: /CallLog/Delete/5

        public ActionResult Delete(int id = 0)
        {
            var calllog = _repo.GetById<CallLog>(id);
            return calllog == null ? (ActionResult) HttpNotFound() : View(calllog);
        }

        //
        // POST: /CallLog/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                _repo.Delete<CallLog>(id);
                _repo.Save();
            }
            catch (Exception ex)
            {
                if (!ex.Message.StartsWith("Violation of UNIQUE KEY constraint")) return RedirectToAction("Index");
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                ModelState.AddModelError("Name", "You cannot delete related data.");
            }
            return RedirectToAction("Index");
        }
    }
}