﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Mvc;
using ProAlert.Andon.Service.Common;
using ProAlert.Andon.Service.Interfaces;
using ProAlert.Andon.Service.Models;
using ProAlert.Andon.Service.ViewModels;

namespace Vistech.Andon.Web.Controllers
{
    //[InitializeSimpleMembership]
    [Authorize(Roles = "Admin")]
    public class WorkCenterController : Controller
    {
        private IRepository _repo;

        public WorkCenterController(IRepository repo)
        {
            _repo = repo;
        }
        //
        // GET: /WorkCenter/

        public ActionResult Index()
        {
            return View(_repo.GetAll<WorkCenter>(orderBy: o => o.OrderBy(x => x.Name)));
        }

        //
        // GET: /WorkCenter/Details/5

        public ActionResult Details(int id = 0)
        {
            var workcenter = _repo.GetById<WorkCenter>(id);
            if (workcenter == null)
            {
                return HttpNotFound();
            }
            return View(workcenter);
        }

        //
        // GET: /WorkCenter/Create

        public ActionResult Create()
        {
            var vm = new WorkCenter {WorkCenterGUIOptions = new WorkCenterGUIOptions()};
            return View(vm);
        }

        //
        // POST: /WorkCenter/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(WorkCenter workcenter)
        {
            if (ModelState.IsValid)
            {
                workcenter.OEEStartTime = DateTime.UtcNow;
                workcenter.GuiRunning = false;
                workcenter.MachineRunning = false;
                _repo.Create(workcenter, User.Identity.Name);
                _repo.Save();
                return RedirectToAction("Index");
            }

            return View(workcenter);
        }

        public ActionResult CreateCalls()
        { 
            var workCenters = _repo.Get<WorkCenter>().ToList();
            var firstId = workCenters[0].Id;
            if (_repo.Get<WorkCenterCall>(x => x.WorkCenterID == firstId).Any()) return View("Index", _repo.GetAll<WorkCenter>(orderBy: o => o.OrderBy(x => x.Name)));
            var calls = _repo.Get<Call>();

            foreach (var wc in workCenters)
            {
                foreach (var call in calls)
                {
                    _repo.Create(new WorkCenterCall
                    {
                        Call = call,
                        WorkCenter = wc,
                        OrderID = call.Name.Equals("Dumpster") ? 1 : 0,
                        CreatedDate = DateTime.UtcNow
                    });
                ;
                }
            }
            _repo.Save();
            return View("Index", _repo.GetAll<WorkCenter>(orderBy: o => o.OrderBy(x => x.Name)));
        }
        //
        // GET: /WorkCenter/Edit/5

        public ActionResult Edit(int id = 0)
        {
            var workcenter = _repo.GetOne<WorkCenter>(x => x.Id == id, includeProperties:"WorkCenterGUIOptions");
            if (workcenter == null)
            {
                return HttpNotFound();
            }
            if (workcenter.WorkCenterGUIOptions == null)
                workcenter.WorkCenterGUIOptions = new WorkCenterGUIOptions();
            return View(workcenter);
        }

        //
        // POST: /WorkCenter/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(WorkCenter workcenter)
        {
            var wcToEdit = _repo.GetOne<WorkCenter>(x => x.Id == workcenter.Id, includeProperties: "WorkCenterGUIOptions");
            if (wcToEdit.WorkCenterGUIOptions == null)
                wcToEdit.WorkCenterGUIOptions = new WorkCenterGUIOptions();


            if (wcToEdit == null) return HttpNotFound();
            
            if (ModelState.IsValid)
            {
                wcToEdit.Description = workcenter.Description;
                wcToEdit.GuiRunning = workcenter.GuiRunning;
                wcToEdit.MachineRunning = workcenter.MachineRunning;
                wcToEdit.WorkCenterGUIOptions.ManualCycles = workcenter.WorkCenterGUIOptions.ManualCycles;
                wcToEdit.WorkCenterGUIOptions.SingleScrap = workcenter.WorkCenterGUIOptions.SingleScrap;

                _repo.Update(wcToEdit, User.Identity.Name);
                _repo.Save();
                return RedirectToAction("Index");
            }
            return View(workcenter);
        }

        //
        // GET: /WorkCenter/Delete/5

        public ActionResult Delete(int id = 0)
        {
            var workcenter = _repo.GetById<WorkCenter>(id);
            ViewBag.Msg = _repo.Get<ProductWorkCenterStat>(x => x.WorkCenterID == id).Any() ? "Cannot delete." : "Are you sure you want to delete?";

            if (workcenter == null)
            {
                return HttpNotFound();
            }
            return View(workcenter);
        }

        //
        // POST: /WorkCenter/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                _repo.Delete<WorkCenter>(id);
                _repo.Save();
            }
            catch (Exception ex)
            {
                if (ex.InnerException.InnerException.Message.StartsWith("The DELETE statement conflicted with the REFERENCE constraint"))
                {
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    ModelState.AddModelError("Name", "You cannot delete related data.");
                }
            }
            return RedirectToAction("Index");
        }

        public ActionResult ManageCalls(int id, string CallTypeID, string dt)
        {
            var wc = _repo.GetById<WorkCenter>(id);
            if (wc == null)
            {
                return HttpNotFound();
            }

            var type = (Enumerations.CallType)Enum.Parse(typeof(Enumerations.CallType), CallTypeID, true);
            var bDT = dt != "0";
            var viewModel = new WorkCenterCallVM
            {
                Calls = _repo.Get<Call>(x => x.Type == type && x.DT == bDT, orderBy: o => o.OrderBy(x => x.DT).ThenBy(x => x.Name)),
                WorkCenterCalls = _repo.Get<WorkCenterCall>(x => x.WorkCenterID == wc.Id && x.Call.Type == type && x.Call.DT == bDT)
                //WorkCenters = wc
            };

            var typeQuery = (from c in _repo.GetAll<Call>()
                             select c.Type).Distinct();


            ViewBag.CallTypeID = new SelectList(typeQuery, "CallTypeID");
            ViewBag.Title = "Manage " + wc.Name + " Calls";
            ViewBag.WCID = wc.Id;
            ViewBag.DT = dt;

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult SaveOrder(List<WorkCenterCall> workCenterCalls, string CallTypeID, string dt, int wcID)
        {
            var type = (Enumerations.CallType)Enum.Parse(typeof(Enumerations.CallType), CallTypeID, true);
            var bDT = dt != "0";

            var shortlist = _repo.Get<WorkCenterCall>(x => x.WorkCenterID == wcID && x.Call.Type == type && x.Call.DT == bDT);
            var statusCode = 0;
            StringBuilder errorMessage = null;

            foreach (var wcc in shortlist.ToList())
            {
                _repo.Delete(wcc);
            }
            _repo.Save();
            // delete all records in WorkCenterCall where WorkCenterID== WorkCenterID and type and dt
            // insert all passed workCenterCalls into WorkCenterCall
            if (workCenterCalls != null)
            {
                if (ModelState.IsValid)
                {
                    try
                    {
                        foreach (var wcc in workCenterCalls)
                        {
                            _repo.Create(wcc, User.Identity.Name);
                        }
                        _repo.Save();
                    }
                    catch (Exception exp)
                    {
                        errorMessage = new StringBuilder(200);
                        errorMessage.AppendFormat(
                            "<div class=\"validation-summary-errors\" title\"Server Error\">{0}</div>",
                            exp.GetBaseException().Message);
                        statusCode = (int)HttpStatusCode.InternalServerError; // = 500
                    }
                } 
            }

            if (Request.IsAjaxRequest())
            {
                if (statusCode > 0)
                {
                    Response.StatusCode = statusCode;
                    return Content(errorMessage.ToString());
                }
                TempData["message"] = "Work Center Call Changes saved.";
                return Json(new { success = true });
            }
            return RedirectToAction("Index");
        }
    }
}