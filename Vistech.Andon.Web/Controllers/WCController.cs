﻿using System;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using ProAlert.Andon.Service.Interfaces;
using ProAlert.Andon.Service.Models;
using ProAlert.Andon.Service.ViewModels;
using Vistech.Andon.Web.Models;
using Vistech.Andon.Web.Tools;

namespace Vistech.Andon.Web.Controllers
{
    //[InitializeSimpleMembership]
    [Authorize(Roles = "Operator, Admin, Tech, Maint, Super")]
    public class WCController : Controller
    {
        //
        // GET: /WC/
        #region < Members >
        //private IRepository _repo;
        private OeeCache _cache;
        #endregion

        //public WCController(IRepository repo)
        //{
            
        //    _repo = repo;
        //    _cache = new OeeCache();
        //}
        public WCController()
        {
        }
        
        public ActionResult WCList()
        {
            using (var context = new ProAlertContext())
            {
                using (var repo = new EntityFrameworkRepository<ProAlertContext>(context))
                {
                    var query = (from wc in repo.GetAll<WorkCenter>()
                                 select wc).ToList();

                    return View(query);
                }
            }
        }

        public ActionResult AlreadyRunning()
        {
            return View();
        }
        public ActionResult WC(int id)
        {
            using (var db = new ProAlertContext())
            {
                using (var repo = new EntityFrameworkRepository<ProAlertContext>(db))
                {
                    var wc = repo.GetOne<WorkCenter>(x => x.Id == id, "WorkCenterGUIOptions");
                    if (wc.GuiRunning) RedirectToAction("AlreadyRunning");
                    // Not running yet... but, when we leave this method, we will be.. so set it.
                    wc.GuiRunning = true;
                    repo.Save();
                    // do we have a new work center? If tl is null, we do.

                    var tl = repo.GetOne<WCProductTimeline>(x => x.WorkCenterID == id && x.Current, "ShiftLog, ProductWCStatsHistory, Timesegments, WorkCenter, Product");
                    ProductTimelineStatsVM tlVm;
                    if (tl == null)
                    {
                        var now = DateTime.UtcNow;

                        tlVm = new ProductTimelineStatsVM(new WCProductTimeline { WorkCenterID = id, WorkCenter = repo.GetById<WorkCenter>(id) }, repo)
                        {
                            WorkCenterID = id
                        };
                        var pdt = new PlannedDtLog
                        {
                            Start = now,
                            PlannedDtID = repo.GetOne<PlannedDt>(x => x.Name.Equals("Product Change Over")).Id,
                            Notes = "WC Startup",
                            WorkCenterID = id
                        };
                        tlVm.CreateNewCurrent(now, pdt);
                        try
                        {
                            tlVm.repo.Save();
                        }
                        catch (Exception ex)
                        {
                            repo.InsertError(new AppError
                            {
                                ErrorDT = now,
                                ErrorMsg = ex.Message,
                                Source = "WCController - Initial setup"
                            });
                            repo.Save();
                        }
                    }
                    else
                    {
                        tlVm = new ProductTimelineStatsVM(tl, repo);
                    }
                    //db.Proxies = true;

                    var viewModel = new WCVM
                    {
                        CallLogs =
                            repo.Get<CallLog>(
                                cla =>
                                    cla.WorkCenterId == id &&
                                    ((cla.Call.DT && cla.ResolveDt == null) || (!cla.Call.DT && cla.ResponseDt == null))),
                        WorkCenter = repo.GetFirst<WorkCenter>(x => x.Id == id, includeProperties: "WorkCenterCalls"),
                        Employee = new Employee(),
                        //WorkCenterCalls = repo.GetAll<WorkCenterCall>(x => x.WorkCenterID = id),
                        Calls = repo.GetAll<Call>()
                    };
                    //db.Proxies = false;
                    var pid = viewModel.WorkCenter.ProductID;

                    ViewBag.PID = pid;
                    var prodName = pid != null ? repo.GetById<Product>(pid).Name : null;
                    ViewBag.ProductName = prodName ?? "Select Product";

                    //PopulateProductDD(id, pid);

                    viewModel.ProductTimelineStatsVm = tlVm; //SetOEEHeader(id, pid);

                    ViewBag.Title = "WC " + viewModel.WorkCenter.Name;
                    var context = new ApplicationDbContext();
                    var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
                    var user = userManager.FindByName("WorkCenter" + viewModel.WorkCenter.Id);
                    if (user != null)
                    {
                        var identity = userManager.CreateIdentity(user, DefaultAuthenticationTypes.ApplicationCookie);
                        var authenticationManager = HttpContext.GetOwinContext().Authentication;
                        authenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = true }, identity);
                    }

                    return View(viewModel);
                }
            }

        }

        //private ProductTimelineStatsVM SetOEEHeader(int workCenterId, int? pid = null)
        //{
            
        //    var oeeHeader = _cache.ReturnWCTL(workCenterId);
        //    // do not attempt to start another update if updating
        //    if (oeeHeader.Updating) return oeeHeader;
        //    var now = DateTime.UtcNow;
        //    if (now.Subtract(oeeHeader.LastUpdate).TotalMilliseconds < 10000) return oeeHeader;
        //    oeeHeader.Updating = true;
        //    oeeHeader.LastUpdate = now;

        //    try
        //    {
        //        var current =
        //            _repo.GetOne<WCProductTimeline>(x => x.WorkCenterID == workCenterId && x.Current,
        //                includeProperties: "ProductWCStatsHistory, WorkCenter, Product");

        //        // need to add some logic to handle if the above comes back with more than one record.
        //        oeeHeader.TL = current;
        //        //current.Current = true;
        //        if (current != null)
        //        {
        //            _repo.Get<Timesegment>(
        //                wc =>
        //                    wc.WCProductTimelineID == current.Id &&
        //                    ((wc.Start <= current.Start && (wc.Stop ?? now) > current.Start) ||
        //                     (wc.Start > current.Start && wc.Start < current.Stop)),
        //                includeProperties: "CycleSummary, CallLogs, PlannedDtLogs, Scraps");

        //            foreach (var ts in current.Timesegments)
        //            {
        //                if (ts.CycleSummaryID != null)
        //                {
        //                    ts.CycleSummary = _repo.GetById<CycleSummary>(ts.CycleSummaryID);
        //                }

        //                if (ts.Scraps != null)
        //                {
        //                    foreach (var s in ts.Scraps)
        //                    {
        //                        s.ScrapType = _repo.GetById<ScrapType>(s.ScrapTypeID);
        //                    }
        //                }

        //                if (ts.PlannedDtLogs != null)
        //                {
        //                    foreach (var p in ts.PlannedDtLogs)
        //                    {
        //                        p.PlannedDt = _repo.GetById<PlannedDt>(p.PlannedDtID);
        //                    }
        //                }
        //                if (ts.CallLogs != null)
        //                {
        //                    foreach (var c in ts.CallLogs)
        //                    {
        //                        c.Call = _repo.GetById<Call>(c.CallID);
        //                    }
        //                }
        //            }
        //            // this should not be set here.
        //            //oeeHeader.TL = current;

        //        }
        //        //db.Configuration.ProxyCreationEnabled = false;
        //        if (oeeHeader.TL != null)
        //        {
        //            OeeCache.CacheWCTL(oeeHeader);
        //            oeeHeader.repo = _repo;
        //            oeeHeader.SetValues(_repo.GetById<WorkCenter>(workCenterId).OEEStartTime, now);
        //            _repo = oeeHeader.repo;
        //            _repo.Update(oeeHeader.TL, User.Identity.Name);
        //            oeeHeader.LastUpdate = now;
        //            oeeHeader.Updating = false;
        //            _repo.Save();
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        _repo.InsertError(new AppError
        //        {
        //            ErrorDT = DateTime.UtcNow,
        //            ErrorMsg = "WcId: " + workCenterId + " -- " + ex.Message,
        //            Source = oeeHeader.WCProduct + ": WCController - Update OEEHeader"
        //        });
        //        _repo.Save();
        //    }
        //    finally
        //    {
        //        oeeHeader.Updating = false;
        //    }
        //    OeeCache.CacheWCTL(oeeHeader);
        //    return oeeHeader;
        //}

        #region Calls

        public JsonResult IsCallActive(int wccid)
        {
            using (var context = new ProAlertContext())
            {
                using (var repo = new EntityFrameworkRepository<ProAlertContext>(context))
                {
                    //db.Configuration.ProxyCreationEnabled = false;
                    var wcc = repo.GetById<WorkCenterCall>(wccid);
                    //db.Configuration.ProxyCreationEnabled = true;
                    if (wcc == null) return Json(false, JsonRequestBehavior.AllowGet);
                    var found = repo.Get<CallLog>(cl => cl.WorkCenterId == wcc.WorkCenterID && cl.CallID == wcc.CallID
                                       && ((cl.Call.DT && cl.ResolveDt == null) || (!cl.Call.DT && cl.ResponseDt == null))).Any();

                    return Json(found, JsonRequestBehavior.AllowGet);
                }
            }
        }

        public ActionResult UpdateCallList(int wcid)
        {
            using (var context = new ProAlertContext())
            {
                using (var repo = new EntityFrameworkRepository<ProAlertContext>(context))
                {
                    var viewModel = new WCVM
                    {
                        CallLogs = repo.Get<CallLog>(cla => (cla.WorkCenterId == wcid && cla.Call.DT && cla.ResolveDt == null) || (cla.WorkCenterId == wcid && !cla.Call.DT && cla.ResponseDt == null), includeProperties: "Call"),
                        WorkCenter = repo.GetById<WorkCenter>(wcid)
                    };

                    if (Request.IsAjaxRequest())
                    {
                        return PartialView("_ActiveCallLogs", viewModel.CallLogs);
                    }
                    return View("WC", viewModel);
                }
            }
        }

        public ActionResult UpdateCallResponse(int calllogid, int empid)
        {
            using (var context = new ProAlertContext())
            {
                using (var repo = new EntityFrameworkRepository<ProAlertContext>(context))
                {
                    var cl = repo.GetById<CallLog>(calllogid);
                    var id = cl.WorkCenterId;

                    // mike doesn't want to record userid when responding to call.
                    // passing -1 for now in case he changes his mind.
                    if (empid != -1)
                        cl.EmployeeID = empid;

                    cl.ResponseDt = DateTime.UtcNow;

                    repo.Save();

                    var viewModel = new WCVM
                    {
                        CallLogs = repo.Get<CallLog>(cla => (cla.WorkCenterId == id && cla.Call.DT && cla.ResolveDt == null) || (cla.WorkCenterId == id && !cla.Call.DT && cla.ResponseDt == null), includeProperties: "Call"),
                        WorkCenter = repo.GetById<WorkCenter>(id)
                    };

                    if (Request.IsAjaxRequest())
                    {
                        return PartialView("_ActiveCallLogs", viewModel.CallLogs);
                    }
                    return View("WC", viewModel);
                }
            }
        }

        public ActionResult CreateNonDtCallLog(int wccid, int pid)
        {
            using (var context = new ProAlertContext())
            {
                using (var repo = new EntityFrameworkRepository<ProAlertContext>(context))
                {
                    var statusCode = 0;
                    StringBuilder errorMessage = null;
                    var wcc = repo.GetById<WorkCenterCall>(wccid);
                    var id = wcc.WorkCenterID;
                    var found = repo.Get<CallLog>(cl => cl.WorkCenterId == wcc.WorkCenterID && cl.CallID == wcc.CallID
                           && ((cl.Call.DT && cl.ResolveDt == null) || (!cl.Call.DT && cl.ResponseDt == null))).Any();
                    if (found) return RedirectToAction("WC");

                    var tlVm = GetTimelineVm(id, repo);
                    tlVm.CreateNonDTCall(wcc, pid);
                    try
                    {
                        tlVm.repo.Save();
                    }
                    catch (Exception ex)
                    {
                        errorMessage = new StringBuilder(200);
                        errorMessage.AppendFormat(
                            "<div class=\"validation-summary-errors\" title\"Server Error\">{0}</div>",
                            ex.GetBaseException().Message);
                        statusCode = (int)HttpStatusCode.InternalServerError; // = 500

                        repo.InsertError(new AppError
                        {
                            ErrorDT = DateTime.UtcNow,
                            ErrorMsg = ex.Message,
                            Source = "WCController - Create NON DT Call Log"
                        });
                        repo.Save();
                    }
                    if (!Request.IsAjaxRequest()) return RedirectToAction("WC");
                    if (statusCode > 0)
                    {
                        Response.StatusCode = statusCode;
                        return Content(errorMessage.ToString());
                    }
                    TempData["message"] = "Call was added.";

                    var viewModel = new WCVM
                    {
                        CallLogs = repo.Get<CallLog>(cla => (cla.WorkCenterId == id && cla.Call.DT && cla.ResolveDt == null) || (cla.WorkCenterId == id && !cla.Call.DT && cla.ResponseDt == null), includeProperties: "Call"),
                        WorkCenter = repo.GetById<WorkCenter>(id)
                    };

                    return PartialView("_ActiveCallLogs", viewModel.CallLogs);
                }
            }

        }

        /// <summary>
        /// Needs Workcenter ID. Returns initialized ProductTimelineStatsVM to do work.
        /// </summary>
        /// <param name="workCenterId"></param>
        /// <param name="repo"></param>
        /// <returns></returns>
        private ProductTimelineStatsVM GetTimelineVm(int workCenterId, IRepository repo)
        {
            var tempTlVm =
                new ProductTimelineStatsVM(
                    repo.GetOne<WCProductTimeline>(x => x.WorkCenterID == workCenterId && x.Current,
                        "ShiftLog, ProductWCStatsHistory, Timesegments, WorkCenter, Product"), repo)
                {
                    User = User.Identity.Name
                };

            return tempTlVm;
        }

        public ActionResult CallLogCreate(int wccid, int pid = 0)
        {
            using (var context = new ProAlertContext())
            {
                using (var repo = new EntityFrameworkRepository<ProAlertContext>(context))
                {
                    // make sure this type of call is not already active.
                    var wcc = repo.GetOne<WorkCenterCall>(x => x.Id == wccid, includeProperties: "Call");
                    var found = repo.Get<CallLog>(cl => cl.WorkCenterId == wcc.WorkCenterID && cl.CallID == wcc.CallID
                        && (cl.Call.DT && cl.ResolveDt == null || !cl.Call.DT && cl.ResponseDt == null)).Any();
                    if (found) return RedirectToAction("WC");

                    var vm = new CallLogCreateVM
                    {
                        CallID = wcc.CallID,
                        ProductID = pid,
                        WorkCenterId = wcc.WorkCenterID,
                        CallName = wcc.Call.Name,
                        Issues = repo.GetAll<Issue>(),
                        SubCategories = repo.GetAll<SubCategory>()
                    };

                    if (Request != null && Request.IsAjaxRequest())
                        return PartialView("_CallLogCreate", vm);
                    return View("_CallLogCreate", vm);
                }
            }
        }

        public ActionResult CloseCallLogGet(int calllogid, int empid)
        {
            using (var context = new ProAlertContext())
            {
                using (var repo = new EntityFrameworkRepository<ProAlertContext>(context))
                {
                    var cl = repo.GetOne<CallLog>(x => x.Id == calllogid, includeProperties: "Call");
                    cl.EmployeeID = empid;

                    var vm = new CallLogCloseVM
                    {
                        CallLog = cl,
                        CallName = cl.Call.Name,
                        Issues = repo.GetAll<Issue>(),
                        SubCategories = repo.GetAll<SubCategory>()
                    };

                    if (Request != null && Request.IsAjaxRequest())
                        return PartialView("_CloseCallLog", vm);
                    return View("_CloseCallLog", vm);
                }
            }
        }

        [HttpPost]
        public ActionResult CallLogCreate(CallLogCreateVM model)
        {
            using (var context = new ProAlertContext())
            {
                using (var repo = new EntityFrameworkRepository<ProAlertContext>(context))
                {
                    var statusCode = 0;
                    StringBuilder errorMessage = null;
                    var id = model.WorkCenterId;

                    if (!ModelState.IsValid)
                    {
                        if (Request.IsAjaxRequest())
                        {
                            Response.StatusCode = 9;

                            ViewBag.CallName = repo.GetById<Call>(model.CallID).Name;

                            return PartialView("_CallLogCreate", model);
                        }
                        return PartialView("_CallLogCreate", model);
                    }
                    var tlVm = GetTimelineVm(id, repo);

                    tlVm.CreateCall(model);
                    try
                    {
                        tlVm.repo.Save();
                    }
                    catch (Exception ex)
                    {
                        errorMessage = new StringBuilder(200);
                        errorMessage.AppendFormat(
                            "<div class=\"validation-summary-errors\" title\"Server Error\">{0}</div>",
                            ex.GetBaseException().Message);
                        statusCode = (int)HttpStatusCode.InternalServerError; // = 500

                        repo.InsertError(new AppError
                        {
                            ErrorDT = DateTime.UtcNow,
                            ErrorMsg = ex.Message,
                            Source = "WCController - CallLogCreate"
                        });
                        repo.Save();
                    }
                    if (Request.IsAjaxRequest())
                    {
                        if (statusCode > 0)
                        {
                            Response.StatusCode = statusCode;
                            return Content(errorMessage.ToString());
                        }
                        TempData["message"] = "Issue was added.";

                        var callLog =
                            repo.Get<CallLog>(
                                cla =>
                                    (cla.WorkCenterId == id && cla.Call.DT && cla.ResolveDt == null) ||
                                    (cla.WorkCenterId == id && !cla.Call.DT && cla.ResponseDt == null),
                                includeProperties: "Call");

                        return PartialView("_ActiveCallLogs", callLog);
                    }
                    return RedirectToAction("WC");
                }
            }

        }

        [HttpPost]
        public ActionResult LaterCallLog(CallLogCloseVM model)
        {
            using (var context = new ProAlertContext())
            {
                using (var repo = new EntityFrameworkRepository<ProAlertContext>(context))
                {
                    StringBuilder errorMessage = null;
                    var id = model.CallLog.WorkCenterId;
                    if (ModelState.IsValid)
                    {

                        var tlVm = GetTimelineVm(id, repo);
                        tlVm.CloseCall(model, false);
                        try
                        {
                            tlVm.repo.Save();
                        }
                        catch (Exception ex)
                        {
                            errorMessage = new StringBuilder(200);
                            errorMessage.AppendFormat(
                                "<div class=\"validation-summary-errors\" title\"Server Error\">{0}</div>",
                                ex.GetBaseException().Message);
                            //statusCode = (int)HttpStatusCode.InternalServerError; // = 500

                            repo.InsertError(new AppError
                            {
                                ErrorDT = DateTime.UtcNow,
                                ErrorMsg = ex.Message,
                                Source = "WCController - Later DT"
                            });
                            repo.Save();
                        }
                    }

                    var viewModel = new WCVM
                    {
                        CallLogs = repo.Get<CallLog>(cla => (cla.WorkCenterId == id && cla.Call.DT && cla.ResolveDt == null) || (cla.WorkCenterId == id && !cla.Call.DT && cla.ResponseDt == null), includeProperties: "Call"),
                        WorkCenter = repo.GetById<WorkCenter>(id)
                    };

                    if (Request.IsAjaxRequest())
                    {
                        ViewBag.CallName = repo.GetById<Call>(model.CallLog.CallID).Name;
                        return PartialView("_ActiveCallLogs", viewModel.CallLogs);
                    }
                    return View("WC", viewModel);
                }
            }

        }

        [HttpPost]
        public ActionResult CloseCallLog(CallLogCloseVM model)
        {
            using (var context = new ProAlertContext())
            {
                using (var repo = new EntityFrameworkRepository<ProAlertContext>(context))
                {
                    StringBuilder errorMessage = null;
                    var id = model.CallLog.WorkCenterId;

                    if (model.CallLog.IssueID <= 0 || model.CallLog.IssueID == null)
                        ModelState.AddModelError("IssueID", "*");
                    if (model.CallLog.SubCategoryID <= 0 || model.CallLog.SubCategoryID == null)
                        ModelState.AddModelError("SubCategoryID", "*");
                    if (model.CallLog.Resolution == null)
                        ModelState.AddModelError("Resolution", "*");
                    if (model.CallLog.Resolution != null)
                        if (model.CallLog.Resolution.Length <= 0)
                            ModelState.AddModelError("Resolution", "*");

                    if (ModelState.IsValid)
                    {
                        var tlVm = GetTimelineVm(id, repo);

                        tlVm.CloseCall(model);
                        try
                        {
                            tlVm.repo.Save();
                        }
                        catch (Exception ex)
                        {
                            errorMessage = new StringBuilder(200);
                            errorMessage.AppendFormat(
                                "<div class=\"validation-summary-errors\" title\"Server Error\">{0}</div>",
                                ex.GetBaseException().Message);
                            //statusCode = (int)HttpStatusCode.InternalServerError; // = 500
                            repo.InsertError(new AppError
                            {
                                ErrorDT = DateTime.UtcNow,
                                ErrorMsg = ex.Message,
                                Source = "WCController - Close DT"
                            });
                            repo.Save();
                        }

                    }
                    else
                    {
                        if (Request.IsAjaxRequest())
                        {
                            model.Issues = repo.GetAll<Issue>();
                            model.SubCategories = repo.GetAll<SubCategory>();
                            model.CallName = repo.GetById<Call>(model.CallLog.CallID).Name;
                            Response.StatusCode = 9;

                            return PartialView("_CloseCallLog", model);
                        }
                        return PartialView("_CloseCallLog", model);
                    }

                    var viewModel = new WCVM
                    {

                        CallLogs = repo.Get<CallLog>(cla => (cla.WorkCenterId == id && cla.Call.DT && cla.ResolveDt == null) || (cla.WorkCenterId == id && !cla.Call.DT && cla.ResponseDt == null), includeProperties: "Call"),
                        WorkCenter = repo.GetById<WorkCenter>(id)
                    };

                    if (Request.IsAjaxRequest())
                    {
                        return PartialView("_ActiveCallLogs", viewModel.CallLogs);
                    }
                    return View("WC", viewModel);
                }
            }

        }
        #endregion

        public JsonResult SetWorkCenterDownFlag(int id, string webDate)
        {
            using (var context = new ProAlertContext())
            {
                using (var repo = new EntityFrameworkRepository<ProAlertContext>(context))
                {
                    var wc = repo.GetById<WorkCenter>(id);
                    if (wc == null)
                    {
                        repo.InsertError(new AppError
                        {
                            ErrorDT = DateTime.UtcNow,
                            ErrorMsg = "Work Center not found setting down flag -- wc:  " + id.ToString(),
                            Source = "WCController.cs SetWorkCenterDownFlag"
                        });
                        repo.Save();
                        return Json(false, JsonRequestBehavior.AllowGet);
                    }
                    if (wc.ProductID == null)
                    {
                        wc.UnplannedDt = true;
                        repo.Save();
                        var prodSetup = repo.Get<ProductWorkCenterStat>(p => p.WorkCenterID == wc.Id).Any();
                        if (!prodSetup)
                            return Json(new { pdt = true, name = "", down = true, msg = "QC Tech must setup products to run on this WC" },
                                JsonRequestBehavior.AllowGet);
                        if (!repo.Get<ShiftStartTimes>().Any())
                            return Json(new { pdt = true, name = "", down = true, msg = "Shift Start Times are missing" },
                                JsonRequestBehavior.AllowGet);

                        return Json(new { pdt = true, down = false, name = "Product Change Over", msg = "Select the product you are running" }, JsonRequestBehavior.AllowGet);
                    }
                    var unplanned = repo.Get<CallLog>(cl => cl.WorkCenterId == id && cl.Call.DT && cl.ResolveDt == null).Any();

                    var planned = repo.GetOne<PlannedDtLog>(p => p.WorkCenterID == id && p.Start != null && p.Stop == null, includeProperties: "PlannedDt");


                    wc.UnplannedDt = unplanned;
                    wc.PlannedDt = planned != null;
                    var btnProduct = string.Empty;
                    var pdtName = string.Empty;
                    if (planned != null)
                        pdtName = planned.PlannedDt.Name;
                    if (pdtName.Equals("Product Change Over"))
                        btnProduct = "Select Product";

                    repo.Save();

                    return Json(new { pdt = wc.PlannedDt, updt = wc.UnplannedDt, name = pdtName, msg = "", pid = wc.ProductID, btnHtml = btnProduct }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        private bool GetValue(string webDate)
        {
            var timeZone = webDate.Split('(')[1].Replace(")", "");
            try
            {
                Session["timeZone"] = timeZone;
                var tzi = TimeZoneInfo.FindSystemTimeZoneById(timeZone);
                Session["tzi"] = tzi;
            }
            catch (Exception)
            {
                using (var context = new ProAlertContext())
                {
                    using (var repo = new EntityFrameworkRepository<ProAlertContext>(context))
                    {
                        if (repo.Get<TzConversion>(x => x.Client == timeZone).Any())
                        {
                            timeZone = repo.GetFirst<TzConversion>(x => x.Client == timeZone).System;
                            var tzi = TimeZoneInfo.FindSystemTimeZoneById(timeZone);
                            Session["tzi"] = tzi;
                            Session["timeZone"] = timeZone;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        #region Time Zone
        [AllowAnonymous]
        public ActionResult TzConversionCreate()
        {
            var timeZone = (string) Session["timeZone"];
            var model = new TzConversion {Client = timeZone};
            ViewBag.System = new SelectList(TimeZoneInfo.GetSystemTimeZones().ToList(), "ID", "DisplayName", null);

            return PartialView("_TzConversionCreate", model);
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult TzConversionCreate([Bind(Include="Client, System")] TzConversion model)
        {
            if (model.System == "<-- select Zone -->")
                ModelState.AddModelError("System", "Select TimeZone");
            var statusCode = 0;
            
            if (ModelState.IsValid)
            {
                using (var context = new ProAlertContext())
                {
                    using (var repo = new EntityFrameworkRepository<ProAlertContext>(context))
                    {
                        repo.Create(model, User.Identity.Name);
                        repo.Save();
                    }
                }
            }
            else
            {
                statusCode = (int)HttpStatusCode.InternalServerError; // = 500
            }
            if (Request.IsAjaxRequest())
            {
                if (statusCode > 0)
                {
                    Response.StatusCode = statusCode;
                    return Content("something went wrong");
                }
                TempData["message"] = "Issue was added.";
                var tzi = TimeZoneInfo.FindSystemTimeZoneById(model.System);
                Session["tzi"] = tzi;
                Session["timeZone"] = model.System;

                return Json(true, JsonRequestBehavior.AllowGet);
            }
            return PartialView("_TzConversionCreate", model);
        }
        [HttpPost]
        [AllowAnonymous]
        public ActionResult SetTzi(string webDate)
        {
            var tzi = (TimeZoneInfo) Session["tzi"];
            if (tzi != null)
                return Json(true, JsonRequestBehavior.AllowGet);
            if (!GetValue(webDate))
                return Json(false, JsonRequestBehavior.AllowGet);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Misc Actions

        public ActionResult SaveCurrentProduct(int workCenterId, int pid)
        {
            using (var context = new ProAlertContext())
            {
                using (var repo = new EntityFrameworkRepository<ProAlertContext>(context))
                {
                    // update workcenter, wcproducttimeline, planneddtlog
                    var tlVm = GetTimelineVm(workCenterId, repo);
                    tlVm.SaveCurrentProduct(workCenterId, pid);
                    try
                    {
                        tlVm.repo.Save();
                        //OeeCache.CacheWCTL(tlVm);
                    }
                    catch (DbEntityValidationException ex)
                    {
                        repo.InsertError(new AppError
                        {
                            ErrorDT = DateTime.UtcNow,
                            ErrorMsg = ex.Message,
                            Source = "WCController : Save Current Product : WC: " + workCenterId + " pid: " + pid
                        });
                    }
                    var statusCode = 0;
                    return Json(statusCode == 0, JsonRequestBehavior.AllowGet);
                }
            }
        }

        public ActionResult DisplayOEEStartTime(int workCenterId)
        {
            using (var context = new ProAlertContext())
            {
                using (var repo = new EntityFrameworkRepository<ProAlertContext>(context))
                {
                    var wc = repo.GetById<WorkCenter>(workCenterId);
                    if (wc == null) return HttpNotFound();
                    return PartialView("_WCOeeStart", wc);
                }
            }
        }

        public ActionResult SetOEEStart(WorkCenter model)
        {
            using (var context = new ProAlertContext())
            {
                using (var repo = new EntityFrameworkRepository<ProAlertContext>(context))
                {
                    // test for any open Planned Dt's or any calls.  Report back to user that all calls and or Planned DT must be closed before a shift change can take place.
                    //  At least for now.
                    if (repo.Get<CallLog>(x => x.WorkCenterId == model.Id && x.Call.DT && x.ResolveDt == null).Any() ||
                        repo.Get<PlannedDtLog>(x => x.WorkCenterID == model.Id && x.Stop == null).Any())
                        ModelState.AddModelError("Name", "Cannot do a shift change until all planned and unplanned DT's have been closed.");
                    if (ModelState.IsValid)
                    {
                        //var wc = repo.GetById<WorkCenter>(model.Id);
                        var tzi = (TimeZoneInfo)Session["tzi"];

                        var dbStartDate = DateTime.UtcNow;
                        //wc.OEEStartTime = dbStartDate;
                        var tlVm = GetTimelineVm(model.Id, repo);
                        tlVm.CloseCurrent(dbStartDate);
                        tlVm.CreateNewCurrent(dbStartDate);
                        var wc = tlVm.repo.GetById<WorkCenter>(model.Id);
                        wc.OEEStartTime = dbStartDate;
                        tlVm.repo.Update(wc, User.Identity.Name);
                        try
                        {
                            tlVm.repo.Save();
                        }
                        catch (Exception ex)
                        {
                            repo.InsertError(new AppError
                            {
                                ErrorDT = DateTime.UtcNow,
                                ErrorMsg = "WcId: " + model.Id + " -- " + ex.Message,
                                Source = "SetOEEStart"
                            });
                            repo.Save();
                        }
                        return Json(timeconversions.UTCtoClient(tzi, dbStartDate).ToString("M/dd/yyyy HH:mm:ss"), JsonRequestBehavior.AllowGet);
                    }
                    Response.StatusCode = 9;
                    return PartialView("_WCOeeStart", model);
                }
            }

        }

        public ActionResult NeedToFlash(int wcid)
        {
            using (var context = new ProAlertContext())
            {
                using (var repo = new EntityFrameworkRepository<ProAlertContext>(context))
                {
                    // are we in downtime and still seeing cycles?
                    var flash = false;
                    var ts = repo.GetFirst<Timesegment>(x => x.WcProductTimeline.WorkCenterID == wcid && x.Stop == null, includeProperties: "CycleSummary");

                    if (ts?.CycleSummary != null && ts.UnplannedDt == true)
                    {
                        if (ts.CycleSummary.Count % 5 == 0 && ts.CycleSummary.Count != 0)
                            flash = true;
                    }
                    return Json(new { startFlashing = flash }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        //public ActionResult UpdatePage(int workCenterId)
        //{
        //    using (var context = new ProAlertContext())
        //    {
        //        using (var repo = new EntityFrameworkRepository<ProAlertContext>(context))
        //        {
        //            var pid = repo.GetById<WorkCenter>(workCenterId).ProductID;
        //            if (pid == null)
        //            {
        //                return Json(new { result = false, msg = "Please select product." }, JsonRequestBehavior.AllowGet);
        //            }
        //            var updOee = GetTimelineVm(workCenterId, repo);
        //            // Header
        //            var headerPartial = RenderRazorViewToString(ControllerContext, "_OEEHeader", updOee.WcOeeHeaderVm);
        //            // Stats
        //            var statsPartial = RenderRazorViewToString(ControllerContext, "_WCStats", updOee.WcStatsVm);
        //            var callLogs =
        //                repo.Get<CallLog>(
        //                    cla =>
        //                        (cla.WorkCenterId == workCenterId && cla.Call.DT && cla.ResolveDt == null) ||
        //                        (cla.WorkCenterId == workCenterId && !cla.Call.DT && cla.ResponseDt == null),
        //                    includeProperties: "Call");
        //            var callLogPartial = RenderRazorViewToString(ControllerContext, "_ActiveCallLogs", callLogs);

        //            return Json(new { result = true, headerPartial, statsPartial, callLogPartial }, JsonRequestBehavior.AllowGet);
        //        }
        //    }
        //}

        public static string RenderRazorViewToString(ControllerContext controllerContext, string viewName, object model)
        {
            controllerContext.Controller.ViewData.Model = model;

            using (var stringWriter = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(controllerContext, viewName);
                var viewContext = new ViewContext(controllerContext, viewResult.View, controllerContext.Controller.ViewData, controllerContext.Controller.TempData, stringWriter);
                viewResult.View.Render(viewContext, stringWriter);
                viewResult.ViewEngine.ReleaseView(controllerContext, viewResult.View);
                return stringWriter.GetStringBuilder().ToString();
            }
        }

        public ActionResult DisplayPinDialog()
        {
            var emp = new Employee();

            return PartialView("_CollectPin", emp);
        }
        [HttpPost]
        public ActionResult CollectPin([Bind(Include = "Pin")] Employee employee)
        {
            using (var context = new ProAlertContext())
            {
                using (var repo = new EntityFrameworkRepository<ProAlertContext>(context))
                {
                    var emp = repo.GetFirst<Employee>(x => x.Pin == employee.Pin);
                    if (emp == null)
                        ModelState.AddModelError("Pin", "Unknown PIN");
                    if (ModelState.IsValid)
                    {
                        return Json(emp.Id, JsonRequestBehavior.AllowGet);
                    }
                    if (Request.IsAjaxRequest())
                    {
                        Response.StatusCode = 9;
                        return PartialView("_CollectPin", emp);
                    }
                    return PartialView("_CollectPin", emp);
                }
            }
            //return Json(emp.EmployeeID, "text/html", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }
        public ActionResult CreateCycle(int workCenterId)
        {
            var mc = new Cycle {WorkCenterID = workCenterId};
            return PartialView("_ManualCycle", mc);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateCycles([Bind(Include="WorkCenterID, Count")] Cycle manualCycle)
        {
            //var statusCode = 0;
            if (manualCycle.Count == 0)
                ModelState.AddModelError("Count", "Must enter a number greater than 0 or Cancel");

            if (ModelState.IsValid)
            {
                using (var context = new ProAlertContext())
                {
                    using (var repo = new EntityFrameworkRepository<ProAlertContext>(context))
                    {
                        // enter cycles into cycle table.
                        var tlVm = GetTimelineVm(manualCycle.WorkCenterID, repo);

                        tlVm.CreateCycles(manualCycle);
                        tlVm.SetValues(tlVm.Start, DateTime.UtcNow);
                        try
                        {
                            tlVm.repo.Save();
                        }
                        catch (Exception ex)
                        {
                            repo.InsertError(new AppError
                            {
                                ErrorDT = DateTime.UtcNow,
                                ErrorMsg = ex.Message,
                                Source = "WCController - Create Cycles"
                            });
                            repo.Save();
                        }
                        return Json(new { data = true }, JsonRequestBehavior.AllowGet);
                    }
                }

            }
            if (Request.IsAjaxRequest())
            {
                Response.StatusCode = 9;
                return PartialView("_ManualCycle", manualCycle);
            }
            return PartialView("_ManualCycle", manualCycle);
        }
        public ActionResult DisplayInsertScrap(int workCenterId, int pid)
        {
            using (var context = new ProAlertContext())
            {
                using (var repo = new EntityFrameworkRepository<ProAlertContext>(context))
                {
                    var vm = new ScrapEntryVM
                    {
                        Scrap = new Scrap {WorkCenterID = workCenterId, ProductID = pid, ScrapCount = 1},
                        ScrapTypes = repo.Get<ScrapType>()
                    };

                    return PartialView("_InsertScrap", vm);
                }
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult InsertScrap(ScrapEntryVM model)
        {
            using (var context = new ProAlertContext())
            {
                using (var repo = new EntityFrameworkRepository<ProAlertContext>(context))
                {
                    if (model.Scrap.ScrapCount == 0)
                        ModelState.AddModelError("Scrap.ScrapCount", "Must enter a number greater than 0 or Cancel");
                    if (ModelState.IsValid)
                    {
                        var tlVm = GetTimelineVm(model.Scrap.WorkCenterID ?? 0, repo);

                        tlVm.InsertScrap(model.Scrap);
                        try
                        {
                            tlVm.repo.Save();
                            //OeeCache.CacheWCTL(tlVm);
                        }
                        catch (Exception ex)
                        {
                            repo.InsertError(new AppError
                            {
                                ErrorDT = DateTime.UtcNow,
                                ErrorMsg = ex.Message,
                                Source = "WCController.cs InsertScrap"
                            });
                            repo.Save();
                        }
                        var lastScrapId = repo.GetFirst<Scrap>(orderBy: o => o.OrderByDescending(x => x.Id));
                        var lastscrapid = lastScrapId == null ? 0 : lastScrapId.Id;
                        var str = lastscrapid.ToString().PadLeft(3, '0');
                        str = str.Substring(str.Length - 2);
                        return Json(new { scrapId = "Scrap ID: " + str }, JsonRequestBehavior.AllowGet);
                    }
                    if (Request.IsAjaxRequest())
                    {
                        Response.StatusCode = 9;
                        model.ScrapTypes = repo.Get<ScrapType>();
                        return PartialView("_InsertScrap", model);
                    }

                    return PartialView("_InsertScrap");
                }
            }
        }
        public ActionResult DisplaySelectProduct(int workCenterId)
        {
            using (var context = new ProAlertContext())
            {
                using (var repo = new EntityFrameworkRepository<ProAlertContext>(context))
                {
                    var vm = repo.Get<ProductWorkCenterStat>(x => x.WorkCenterID == workCenterId, o => o.OrderBy(x => x.Product.Name), "Product");
                    return PartialView("_WCProductSelectVM", vm);
                }
            }
        }
        public ActionResult DisplayCurrentProduct(int workCenterId)
        {
            using (var context = new ProAlertContext())
            {
                using (var repo = new EntityFrameworkRepository<ProAlertContext>(context))
                {
                    //Retrieve the current WorkCenter obj in order to get the currently set ProductID
                    var wc = repo.GetById<WorkCenter>(workCenterId);
                    var curPid = wc.ProductID;

                    //Retrieve the Product Obj based on the WorkCneter's ProductID
                    var cP = repo.GetById<Product>(curPid);

                    //Pass the current Product obj to the view
                    return PartialView("_WCProductDisplayVM", cP);
                }
            }
        }

        [HttpPost]
        public ActionResult SelectProduct(int workCenterId, int pid)
        {
            using (var context = new ProAlertContext())
            {
                using (var repo = new EntityFrameworkRepository<ProAlertContext>(context))
                {
                    if (ModelState.IsValid)
                    {
                        SaveCurrentProduct(workCenterId, pid);
                        return Json(new { pid }, JsonRequestBehavior.AllowGet);
                    }

                    var vm = repo.Get<ProductWorkCenterStat>(x => x.WorkCenterID == workCenterId, o => o.OrderBy(x => x.Product.Name), "Product");
                    return PartialView("_WCProductSelectVM", vm);
                }
            }
        }
        #endregion

        #region Planned DT
        public JsonResult AnyOpenPlannedDt(int workCenterId)
        {
            using (var context = new ProAlertContext())
            {
                using (var repo = new EntityFrameworkRepository<ProAlertContext>(context))
                {
                    var found = repo.Get<PlannedDtLog>(p => p.WorkCenterID == workCenterId && p.Start != null && p.Stop == null).Any();

                    return Json(found, JsonRequestBehavior.AllowGet);
                }
            }
        }
        public ActionResult StartPlanned(int workCenterId)
        {
            using (var context = new ProAlertContext())
            {
                using (var repo = new EntityFrameworkRepository<ProAlertContext>(context))
                {
                    var wc = repo.GetById<WorkCenter>(workCenterId);
                    if (wc == null) return HttpNotFound();

                    var pdtLog = new PlannedDtLogVM()
                    {
                        PlannedDtLog = new PlannedDtLog { WorkCenterID = workCenterId },
                        PlannedDts = repo.Get<PlannedDt>()
                    };

                    return PartialView("_PlannedDT", pdtLog);
                }
            }
        }
        [HttpPost]
        public ActionResult StartPlannedDT(PlannedDtLogVM model)
        {
            using (var context = new ProAlertContext())
            {
                using (var repo = new EntityFrameworkRepository<ProAlertContext>(context))
                {
                    if (model.PlannedDtLog.PlannedDtID < 1)
                        ModelState.AddModelError("PlannedDtLog.PlannedDtID", "Select a reason");

                    if (!repo.Get<ShiftStartTimes>().Any())
                    {
                        ModelState.AddModelError("PlannedDtLog.PlannedDtID", "Shift Start Times are missing");
                        return PartialView("_PlannedDT", model);
                    }

                    if (repo.GetById<WorkCenter>(model.PlannedDtLog.WorkCenterID).PlannedDt ?? false)
                    {
                        ModelState.AddModelError("PlannedDtLog.PlannedDtID", "Work Center is already in a planned down time state");
                        return PartialView("_PlannedDT", model);
                    }
                    // Cannot do a product change over with an open DT call.
                    if (repo.Get<CallLog>(x => x.WorkCenterId == model.PlannedDtLog.WorkCenterID && x.Call.DT && x.ResolveDt == null).Any() && repo.GetById<PlannedDt>(model.PlannedDtLog.PlannedDtID).Name.Equals("Product Change Over"))
                    {
                        ModelState.AddModelError("PlannedDtLog.PlannedDtID", "Cannot change a product with an open call");
                    }

                    if (ModelState.IsValid)
                    {
                        var tlVm = GetTimelineVm(model.PlannedDtLog.WorkCenterID, repo);
                        tlVm.StartPlannedDt(model.PlannedDtLog);
                        try
                        {
                            tlVm.repo.Save();
                        }
                        catch (Exception ex)
                        {
                            repo.InsertError(new AppError
                            {
                                ErrorDT = DateTime.UtcNow,
                                ErrorMsg = ex.Message,
                                Source = "WCController - Start Planned DT"
                            });
                            repo.Save();
                        }
                        var name = repo.GetById<PlannedDt>(model.PlannedDtLog.PlannedDtID).Name;
                        return Json(name, JsonRequestBehavior.AllowGet);
                    }
                    model.PlannedDts = repo.Get<PlannedDt>();

                    if (Request.IsAjaxRequest())
                    {
                        Response.StatusCode = 9;
                        return PartialView("_PlannedDT", model);
                    }
                    return PartialView("_PlannedDT");
                }
            }

        }
        public ActionResult StopPlanned(int workCenterId)
        {
            using (var context = new ProAlertContext())
            {
                using (var repo = new EntityFrameworkRepository<ProAlertContext>(context))
                {
                    var wc = repo.GetById<WorkCenter>(workCenterId);

                    if (wc == null) return HttpNotFound();
                    // find open Planned DT event by wc
                    var pdt = repo.GetFirst<PlannedDtLog>(p => p.WorkCenterID == wc.Id
                              && p.Start != null
                              && p.Stop == null, includeProperties: "PlannedDt");

                    //pdt.ProductID = pid;
                    var vm = new StoppedPlannedDtVM
                    {
                        Name = pdt.PlannedDt.Name,
                        PlannedDtLog = pdt
                    };
                    return PartialView("_StopPlannedDT", vm);
                }
            }
        }
        [HttpPost]
        public ActionResult StopPlannedDT(StoppedPlannedDtVM model)
        {
            if (model.Name == null || model.PlannedDtLog == null) return PartialView("_StopPlannedDT", model);
            using (var context = new ProAlertContext())
            {
                using (var repo = new EntityFrameworkRepository<ProAlertContext>(context))
                {
                    var tlVm = GetTimelineVm(model.PlannedDtLog.WorkCenterID, repo);
                    if (tlVm.TL.ProductID == null)
                        ModelState.AddModelError("Name", "Please select product before starting.");
                    tlVm.StopPlannedDt(model.PlannedDtLog);
                    if (ModelState.IsValid)
                    {
                        try
                        {
                            tlVm.repo.Save();
                            //OeeCache.CacheWCTL(tlVm);
                        }
                        catch (Exception ex)
                        {
                            repo.InsertError(new AppError
                            {
                                ErrorDT = DateTime.UtcNow,
                                ErrorMsg = ex.Message,
                                Source = "WCController - Stop Planned DT"
                            });
                            repo.Save();
                        }
                        var d = "dummy"; // timeconversions.UTCtoClient((TimeZoneInfo)Session["tzi"], tlVm.Start).ToString("M/dd/yyyy HH:mm:ss");
                        return Json(d, JsonRequestBehavior.AllowGet);
                    }
                    Response.StatusCode = 9;
                    return PartialView("_StopPlannedDT", model);
                }
            }
        }
        #endregion
        
    }
}
