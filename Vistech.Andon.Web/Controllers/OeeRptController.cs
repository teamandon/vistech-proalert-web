﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using OfficeOpenXml;
using ProAlert.Andon.Service.Interfaces;
using ProAlert.Andon.Service.Models;
using ProAlert.Andon.Service.ViewModels;
using Vistech.Andon.Web.Tools;
using timeconversions = ProAlert.Andon.Service.Common.timeconversions;


namespace Vistech.Andon.Web.Controllers
{
    [Authorize(Roles = "Admin")]

    public class OeeRptController : Controller
    {
        private IRepository _repo;

        public OeeRptController(IRepository repo)
        {
            _repo = repo;
        }
        public ActionResult Index()
        {
            var vm = new RptDateTimeRange
            {
                StartDate = DateTime.Now.Date.AddDays(-1),
                StartTime = "07:00",
                StopDate = DateTime.Now.Date,
                StopTime = "07:00"
            };

            return View(vm);
        }
        // GET: OeeRpt
        [HttpPost]
        public ActionResult Index(RptDateTimeRange model)
        {
            var tzi = (TimeZoneInfo)Session["tzi"];
            var dbStartDate = timeconversions.ClientToUTC(tzi, model.StartDate, model.StartTime);
            var dbStopDate = timeconversions.ClientToUTC(tzi, model.StopDate, model.StopTime);
            var data = new OeeRpts {Repository = _repo};
            var obj = new TlRpt();
            var listTls = obj.GetTls(_repo.Get<WorkCenter>().ToList(), dbStartDate, dbStopDate);
            data.AddByTl(listTls);

            var tbl = data.ConvertToDataTableWithTZ(data.Get());
            var workbook = new ExcelPackage();
            var ws = workbook.Workbook.Worksheets.Add("OEE Data");
            ws.Cells.LoadFromDataTable(tbl, true);
            DPos(tbl, ref ws);

            var wsScrap = workbook.Workbook.Worksheets.Add("Scrap Data");
            var scrapData = new ScrapRpts { Repository = _repo };
            scrapData.AddByTl(listTls);
            tbl = scrapData.ConvertToDataTableWithTZ(scrapData.Get());

            wsScrap.Cells.LoadFromDataTable(tbl, true);
            // find position of DateTime columns: DataTable is zero based
            DPos(tbl, ref wsScrap);

            var wsUpDt = workbook.Workbook.Worksheets.Add("Unplanned DTs");
            var upDtData = new UnplannedDtRpts { Repository = _repo };
            upDtData.AddByTl(listTls);
            tbl = upDtData.ConvertToDataTableWithTZ(upDtData.Get());
            wsUpDt.Cells.LoadFromDataTable(tbl, true);
            DPos(tbl, ref wsUpDt);

            var wsPlannedDt = workbook.Workbook.Worksheets.Add("Planned DTs");
            var plannedDtData = new PlannedDtRpts {Repository = _repo};
            plannedDtData.AddByTl(listTls);
            tbl = plannedDtData.ConvertToDataTableWithTZ(plannedDtData.Get());
            wsPlannedDt.Cells.LoadFromDataTable(tbl, true);
            DPos(tbl, ref wsPlannedDt);


            var wsFreeCall = workbook.Workbook.Worksheets.Add("Free Andon Calls");
            var freeCallData = new FreeAndonCalls { Repository = _repo };
            freeCallData.AddByTl(listTls);
            tbl = freeCallData.ConvertToDataTableWithTZ(freeCallData.Get());
            wsFreeCall.Cells.LoadFromDataTable(tbl, true);
            DPos(tbl, ref wsFreeCall);

            workbook.Save();

            var rptName = dbStartDate.Month.ToString().PadLeft(2, '0') +
                          dbStartDate.Day.ToString().PadLeft(2, '0') + dbStartDate.Year.ToString().PadLeft(2, '0') + "(" + model.StartTime + ")" + " - " + dbStopDate.Month.ToString().PadLeft(2, '0') +
                          dbStopDate.Day.ToString().PadLeft(2, '0') + dbStopDate.Year.ToString().PadLeft(2, '0') + "(" + model.StopTime + ")";
            using (var memoryStream = new MemoryStream())
            {
                workbook.SaveAs(memoryStream);
                memoryStream.Position = 0;
                var memData = memoryStream.ToArray();
                var fileName = "OEE Data - " + rptName + ".xlsx";

                var result = new FileContentResult(memData, "application/vnd.ms-excel") { FileDownloadName = fileName };
                return result;
            }
        }

        private static void DPos(DataTable tbl, ref ExcelWorksheet ws)
        {
            var dPos = new List<int>();
            for (var i = 0; i < tbl.Columns.Count; i++)
                if (tbl.Columns[i].DataType.Name.Equals("DateTime"))
                    dPos.Add(i);
            foreach (var pos in dPos)
            {
                ws.Column(pos + 1).Style.Numberformat.Format = "mm/dd/yyyy hh:mm:ss AM/PM";
            }
        }
    }
}