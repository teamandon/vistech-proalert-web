﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using ProAlert.Andon.Service.Interfaces;
using ProAlert.Andon.Service.Models;
using ProAlert.Andon.Service.ViewModels;
using Vistech.Andon.Web.Tools;

namespace Vistech.Andon.Web.Controllers
{
    //[InitializeSimpleMembership]
    [Authorize(Roles = "Admin, Tech, Maint, Super")]
    public class CallManagementCenterController : Controller
    {
        #region < Members >
        private readonly IRepository _repo;
        private ErrorRx _rx;
        #endregion
        //
        // GET: /CallManagementCenter/
        //private UnitOfWork db = new UnitOfWork();
        public CallManagementCenterController(IRepository repo)
        {
            _repo = repo;
            _rx = new ErrorRx { Repo = _repo };
        }
        public ActionResult CallManagementCenter(int? id = null)
        {
            var name = User.Identity.Name;

            var employee = _repo.GetFirst<Employee>(x => x.LogIn.Trim() == name);

            if (employee == null) return View(new CMCVM
            {
                Calls = new List<Call>(),
                CallLogs = new List<CallLog>(),
                Issues = _repo.GetAll<Issue>(),
                SubCategories = _repo.GetAll<SubCategory>(),
                Employee = new Employee()
            });
            //db.Configuration.ProxyCreationEnabled = false;
            var viewModel = new CMCVM
            {
                Calls = _repo.GetAll<Call>(),
                CallLogs = _repo.Get<CallLog>(x=>(x.EmployeeID == employee.Id || x.EmployeeID == null) && x.Call.DT && x.SignOffDT == null, includeProperties: "Call, WorkCenter", orderBy: s => s.OrderByDescending(o => o.Id)),
                Issues = _repo.GetAll<Issue>(),
                SubCategories = _repo.GetAll<SubCategory>(),
                Employee = employee
            };

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult CloseCallLog(CallLogCloseVM model)
        {
            if (model.CallLog.ResponseDt == null)
                ModelState.AddModelError("CallName", "Must respond at WC");
            if (model.CallLog.ResolveDt == null)
                ModelState.AddModelError("CallName", "Must close call at WC");

            var clToUpdate = _repo.GetOne<CallLog>(x => x.Id == model.CallLog.Id, includeProperties:"Call");

            if (clToUpdate == null) return HttpNotFound();

            if (model.CallLog.IssueID <= 0 || model.CallLog.IssueID == null)
                ModelState.AddModelError("CallLog.IssueID", "*");
            if (model.CallLog.SubCategoryID <= 0 || model.CallLog.SubCategoryID == null)
                ModelState.AddModelError("CallLog.SubCategoryID", "*");
            if (model.CallLog.Resolution == null)
                ModelState.AddModelError("CallLog.Resolution", "*");
            if (model.CallLog.Resolution?.Length <= 0)
                ModelState.AddModelError("CallLog.Resolution", "*");

            if (ModelState.IsValid)
            {
                clToUpdate.SignOffDT = DateTime.UtcNow;
                clToUpdate.Resolution = model.CallLog.Resolution;
                clToUpdate.IssueID = model.CallLog.IssueID;
                clToUpdate.SubCategoryID = model.CallLog.SubCategoryID;
                clToUpdate.EmployeeID = model.CallLog.EmployeeID;

                _repo.Update(clToUpdate, User.Identity.Name);

                try
                {
                    _repo.Save();
                }
                catch (DbUpdateException ex)
                {
                    _rx.RecordError(ex.Message, "CallManagment", "CloseCallLog", model.CallLog.EmployeeID ?? 0);
                    _rx.Repo.Save();
                }
            }
            else
            {
                if (Request.IsAjaxRequest())
                {

                    var vm = new CallLogCloseVM
                    {
                        CallLog = model.CallLog,
                        CallName = clToUpdate.Call.Name,
                        Issues = _repo.GetAll<Issue>(),
                        SubCategories = _repo.GetAll<SubCategory>()
                    };

                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    return PartialView("_CloseCallLog", vm);
                }
                return PartialView("_CloseCallLog", model);
            }

            var viewModel = new CMCVM
            {
                Calls = _repo.GetAll<Call>(),
                CallLogs = _repo.Get<CallLog>(x => (x.EmployeeID == model.CallLog.EmployeeID || x.EmployeeID == null) && x.Call.DT && x.SignOffDT == null, includeProperties: "Call, WorkCenter", orderBy: s => s.OrderByDescending(o => o.Id)),
                Issues = _repo.GetAll<Issue>(),
                SubCategories = _repo.GetAll<SubCategory>(),
                Employee = _repo.GetById<Employee>(model.CallLog.EmployeeID)
            };

            if (Request.IsAjaxRequest())
            {
                return PartialView("CallManagementCenter", viewModel);
            }
            return View("CallManagementCenter", viewModel);
        }

        [HttpPost]
        public ActionResult LaterCallLog(CallLogCloseVM model)
        {
            var clToUpdate = _repo.GetOne<CallLog>(x => x.Id == model.CallLog.Id, includeProperties: "Call");
            
            if (ModelState.IsValid)
            {
                clToUpdate.Resolution = model.CallLog.Resolution;
                clToUpdate.IssueID = model.CallLog.IssueID;
                clToUpdate.SubCategoryID = model.CallLog.SubCategoryID;
                clToUpdate.Planned = model.CallLog.Planned;
                clToUpdate.EmployeeID = model.CallLog.EmployeeID;

                _repo.Update(clToUpdate, User.Identity.Name);

                try
                {
                    _repo.Save();
                }
                catch (DbUpdateException ex)
                {
                    _rx.RecordError(ex.Message, "CallManagment", "LaterCallLog", model.CallLog.EmployeeID ?? 0);
                }
            }
            else
            {
                if (Request.IsAjaxRequest())
                {

                    var vm = new CallLogCloseVM
                    {
                        CallLog = model.CallLog,
                        CallName = clToUpdate.Call.Name,
                        Issues = _repo.GetAll<Issue>(),
                        SubCategories = _repo.GetAll<SubCategory>()
                    };

                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    return PartialView("_CloseCallLog", vm);
                }
                return PartialView("_CloseCallLog", model);
            }

            var viewModel = new CMCVM
            {
                Calls = _repo.GetAll<Call>(),
                CallLogs = _repo.Get<CallLog>(x => (x.EmployeeID == model.CallLog.EmployeeID || x.EmployeeID == null) && x.Call.DT && x.SignOffDT == null, includeProperties: "Call, WorkCenter", orderBy: s => s.OrderByDescending(o => o.Id)),
                Issues = _repo.GetAll<Issue>(),
                SubCategories = _repo.GetAll<SubCategory>(),
                Employee = _repo.GetById<Employee>(model.CallLog.EmployeeID)

            };

            if (Request.IsAjaxRequest())
            {
                return PartialView("CallManagementCenter", viewModel);
            }
            return View("CallManagementCenter", viewModel);
        }


        //private void PopulateCallDD(object selectedCall = null)
        //{
        //    var callQuery = from c in _repo.GetAll<Call>()
        //                    orderby c.Name
        //                    select c;
        //    ViewBag.CallID = new SelectList(callQuery, "CallID", "Name", selectedCall);
        //}

        //private void PopulateIssueDD(object selectedIssue = null)
        //{
        //    var issueQuery = from i in _repo.GetAll<Issue>()
        //                     orderby i.Name
        //                     select i;
        //    ViewBag.IssueID = new SelectList(issueQuery, "IssueID", "Name", selectedIssue);
        //}

        //private void PopulateSubCategoryDD(object selectedSubCat = null)
        //{
        //    var subcatQuery = from sc in _repo.GetAll<SubCategory>()
        //                      orderby sc.Name
        //                      select sc;
        //    ViewBag.SubCategoryID = new SelectList(subcatQuery, "SubCategoryID", "Name", selectedSubCat);
        //}

        //private void PopulateProductDD(int wcid, object selectedProduct = null)
        //{
        //    var productQuery = from a in _repo.Get<WorkCenter>(wc => wc.Id == wcid,
        //        includeProperties: "Product, ProductWorkCenterStats")
        //        select new {a.ProductID, a.Product.Name};

        //    ViewBag.ProductID = new SelectList(productQuery, "ProductID", "Name", selectedProduct);
        //}
    }
}
