﻿using System;
using System.Net;
using System.Web.Mvc;
using ProAlert.Andon.Service.Interfaces;
using ProAlert.Andon.Service.Models;


namespace Vistech.Andon.Web.Controllers
{
    //[InitializeSimpleMembership]
    [Authorize(Roles = "Admin")]
    public class PositionController : Controller
    {
        private IRepository _repo;

        public PositionController(IRepository repo)
        {
            _repo = repo;
        }
        //
        // GET: /Position/

        public ActionResult Index()
        {
            return View(_repo.GetAll<Position>());
        }

        //
        // GET: /Position/Details/5

        public ActionResult Details(int id = 0)
        {
            var position = _repo.GetById<Position>(id);
            if (position == null)
            {
                return HttpNotFound();
            }
            return View(position);
        }

        //
        // GET: /Position/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Position/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id, Title")] Position position)
        {
            if (ModelState.IsValid)
            {
                _repo.Create(position, User.Identity.Name);
                _repo.Save();
                return RedirectToAction("Index");
            }

            return View(position);
        }

        //
        // GET: /Position/Edit/5

        public ActionResult Edit(int id)
        {
            var position = _repo.GetById<Position>(id);
            if (position == null)
            {
                return HttpNotFound();
            }
            return View(position);
        }

        //
        // POST: /Position/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Id, Title")] Position position)
        {
            if (ModelState.IsValid)
            {
                _repo.Update(position);
                _repo.Save();
                return RedirectToAction("Index");
            }
            return View(position);
        }

        //
        // GET: /Position/Delete/5

        public ActionResult Delete(int id = 0)
        {
            var position = _repo.GetById<Position>(id);
            if (position == null)
            {
                return HttpNotFound();
            }
            return View(position);
        }

        //
        // POST: /Position/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                _repo.Delete<Position>(id);
                _repo.Save();
            }
            catch (Exception ex)
            {
                if (ex.Message.StartsWith("Violation of UNIQUE KEY constraint"))
                {
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    ModelState.AddModelError("Name", "You cannot delete related data.");
                }
            }

            return RedirectToAction("Index");
        }

    }
}