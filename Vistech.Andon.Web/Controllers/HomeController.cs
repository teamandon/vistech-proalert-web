﻿using System;
using System.Web.Mvc;
using ProAlert.Andon.Service.Interfaces;
using ProAlert.Andon.Service.Models;


namespace Vistech.Andon.Web.Controllers
{
    public class HomeController : Controller
    {
        private IRepository _repo;

        public HomeController(IRepository repo)
        {
            _repo = repo;
        }
        public ActionResult Index()
        {
            if (User.Identity.Name != "")
            {
                var userTimezone = _repo.GetOne<Employee>(x => x.LogIn.Equals(User.Identity.Name)).TimeZone;
                System.Web.HttpContext.Current.Session["tzi"] = TimeZoneInfo.FindSystemTimeZoneById(userTimezone);
            }


            if (User.IsInRole("Admin") || User.IsInRole("Super"))
            {
                return RedirectToAction("CallManagementCenter", "CallManagementCenter");
            }
            if (User.IsInRole("Operator"))
            {
                return RedirectToAction("WCList", "WCList");
            }
            if (User.IsInRole("AndonDisplay"))
                return RedirectToAction("ADMain", "ADMain");
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }
        [Authorize]
        public ActionResult Contact()
        {
            ViewBag.Message = "Bridge Software Technologies";

            return View();
        }
    }
}