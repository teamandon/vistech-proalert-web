﻿using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Vistech.Andon.Model.Models;
using Vistech.Andon.Service.Common;
using Vistech.Andon.Service.ViewModels;

namespace Vistech.Andon.Web.Controllers
{
    //[InitializeSimpleMembership]
    [Authorize(Roles = "Admin")]
    public class WorkCenterCallController : Controller
    {
        private UnitOfWork db = new UnitOfWork();

        //
        // GET: /WorkCenterCall/

        public ActionResult Index()
        {
            var workcentercalls = db.WorkCenterCallRepository.Get(includeProperties: "WorkCenter, Call");
            return View(workcentercalls.ToList());
        }

        //
        // GET: /WorkCenterCall/Details/5

        public ActionResult Details(int id = 0)
        {
            WorkCenterCall workcentercall = db.WorkCenterCallRepository.GetByID(id);
            if (workcentercall == null)
            {
                return HttpNotFound();
            }
            return View(workcentercall);
        }

        //
        // GET: /WorkCenterCall/Create

        public ActionResult Create()
        {
            var viewModel = new WorkCenterCallVM
            {
                Calls = db.CallRepository.Get(orderBy: o => o.OrderBy(x => x.Type).ThenBy(x => x.Name)),
                WorkCenterCalls = db.WorkCenterCallRepository.Get(x => x.WorkCenterID == 1),
                WorkCenters = db.WorkCenters.Where(x => x.WorkCenterID == 1)
            };
            ViewBag.Title = "Manage Press 1 Calls";
            return View(viewModel);
        }

        // Moved ManangeWorkCenterCalls to WorkCenterController.

        //public ActionResult ManageWorkCenterCalls(WorkCenter wc)
        //{
        //    var viewModel = new WorkCenterCallVM
        //    {
        //        Calls = db.Calls.OrderBy(x => x.Type).ThenBy(x => x.Name),
        //        WorkCenterCalls = db.WorkCenterCalls.Where(x => x.WorkCenterID == wc.WorkCenterID),
        //        WorkCenters = db.WorkCenters.Where(x => x.WorkCenterID == wc.WorkCenterID)
        //    };
        //    var typeQuery = (from c in db.Calls
        //        select c.Type).Distinct();

        //    ViewBag.CallTypeID = new SelectList(typeQuery, "CallTypeID");
        //    ViewBag.Title = "Manage " + wc.Name + " Calls";
        //    return View(viewModel);
        //}
        //
        // POST: /WorkCenterCall/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(WorkCenterCall workcentercall)
        {
            if (ModelState.IsValid)
            {
                db.WorkCenterCalls.Add(workcentercall);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.WorkCenterID = new SelectList(db.WorkCenters, "WorkCenterID", "Name", workcentercall.WorkCenterID);
            ViewBag.CallID = new SelectList(db.Calls, "CallID", "Name", workcentercall.CallID);
            return View(workcentercall);
        }

        //
        // GET: /WorkCenterCall/Edit/5

        public ActionResult Edit(int id = 0)
        {
            WorkCenterCall workcentercall = db.WorkCenterCalls.Find(id);
            if (workcentercall == null)
            {
                return HttpNotFound();
            }
            ViewBag.WorkCenterID = new SelectList(db.WorkCenters, "WorkCenterID", "Name", workcentercall.WorkCenterID);
            ViewBag.CallID = new SelectList(db.Calls, "CallID", "Name", workcentercall.CallID);
            return View(workcentercall);
        }

        //
        // POST: /WorkCenterCall/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(WorkCenterCall workcentercall)
        {
            if (ModelState.IsValid)
            {
                db.Entry(workcentercall).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.WorkCenterID = new SelectList(db.WorkCenters, "WorkCenterID", "Name", workcentercall.WorkCenterID);
            ViewBag.CallID = new SelectList(db.Calls, "CallID", "Name", workcentercall.CallID);
            return View(workcentercall);
        }

        //
        // GET: /WorkCenterCall/Delete/5

        public ActionResult Delete(int id = 0)
        {
            WorkCenterCall workcentercall = db.WorkCenterCalls.Find(id);
            if (workcentercall == null)
            {
                return HttpNotFound();
            }
            return View(workcentercall);
        }

        //
        // POST: /WorkCenterCall/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            WorkCenterCall workcentercall = db.WorkCenterCalls.Find(id);
            db.WorkCenterCalls.Remove(workcentercall);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        //[HttpPost]
        //public ActionResult SaveOrder(List<WorkCenterCall> workCenterCalls)
        //{
        //    var wcID = workCenterCalls[0].WorkCenterID;
        //    var shortlist = db.WorkCenterCalls.Where(x => x.WorkCenterID == wcID);
        //    var statusCode = 0;
        //    StringBuilder errorMessage = null;

        //    foreach (var wcc in shortlist.ToList())
        //    {
        //        db.WorkCenterCalls.Remove(wcc);
        //    }
        //    db.SaveChanges();
        //    // delete all records in WorkCenterCall where WorkCenterID== WorkCenterID
        //    // insert all passed workCenterCalls into WorkCenterCall
        //    try
        //    {
        //        foreach (var wcc in workCenterCalls)
        //        {
        //            db.WorkCenterCalls.Add(wcc);
        //        }
        //        db.SaveChanges();
        //    }
        //    catch (Exception exp)
        //    {
        //        errorMessage = new StringBuilder(200);
        //        errorMessage.AppendFormat(
        //            "<div class=\"validation-summary-errors\" title\"Server Error\">{0}</div>",
        //            exp.GetBaseException().Message);
        //        statusCode = (int)HttpStatusCode.InternalServerError; // = 500
        //    }

        //    if (Request.IsAjaxRequest())
        //    {
        //        if (statusCode > 0)
        //        {
        //            Response.StatusCode = statusCode;
        //            return Content(errorMessage.ToString());
        //        }
        //        else
        //        {
        //            TempData["message"] = "Work Center Call Changes saved.";
        //            return Json(new { success = true});
        //        }
        //    }
        //    else
        //    {
        //        return RedirectToAction("WorkCenter");
        //    }
        //}

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}