﻿using System;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using ProAlert.Andon.Service.Interfaces;
using ProAlert.Andon.Service.Models;

namespace Vistech.Andon.Web.Controllers
{
    public class PlannedDtController : Controller
    {
        private IRepository _repo;

        public PlannedDtController(IRepository repo)
        {
            _repo = repo;
        }
        //
        // GET: /PlannedDt/

        public ActionResult Index()
        {
            return View(_repo.GetAll<PlannedDt>());
        }

        //
        // GET: /PlannedDt/Details/5

        public ActionResult Details(int id = 0)
        {
            var planneddt = _repo.GetById<PlannedDt>(id);
            if (planneddt == null)
            {
                return HttpNotFound();
            }
            return View(planneddt);
        }

        //
        // GET: /PlannedDt/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /PlannedDt/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PlannedDt planneddt)
        {
            if (planneddt.Name.Trim().Length == 0)
                ModelState.AddModelError("Name", "Must enter a Name");
            if (_repo.Get<PlannedDt>(x => x.Name.Equals(planneddt.Name.Trim())).Any())
                ModelState.AddModelError("Name", "Already exists");
            if (ModelState.IsValid)
            {
                _repo.Create(planneddt, User.Identity.Name);
                _repo.Save();
                return RedirectToAction("Index");
            }

            return View(planneddt);
        }

        //
        // GET: /PlannedDt/Edit/5

        public ActionResult Edit(int id = 0)
        {
            var planneddt = _repo.GetById<PlannedDt>(id);
            if (planneddt == null)
            {
                return HttpNotFound();
            }
            return View(planneddt);
        }

        //
        // POST: /PlannedDt/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(PlannedDt planneddt)
        {
            if (ModelState.IsValid)
            {
                var toUpdPlanneddt = _repo.GetById<PlannedDt>(planneddt.Id);
                toUpdPlanneddt.Description = planneddt.Description.Trim();
                _repo.Update(toUpdPlanneddt, User.Identity.Name);
                _repo.Save();
                return RedirectToAction("Index");
            }
            return View(planneddt);
        }

        //
        // GET: /PlannedDt/Delete/5

        public ActionResult Delete(int id = 0)
        {
            var planneddt = _repo.GetById<PlannedDt>(id);
            ViewBag.Msg = _repo.Get<PlannedDtLog>(x => x.PlannedDtID == id).Any() ? "Cannot delete." : "Are you sure you want to delete?";

            if (planneddt == null)
            {
                return HttpNotFound();
            }
            return View(planneddt);
        }

        //
        // POST: /PlannedDt/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                _repo.Delete<PlannedDt>(id);
                _repo.Save();
            }
            catch (Exception ex)
            {
                if (ex.Message.StartsWith("Violation of UNIQUE KEY constraint"))
                {
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    ModelState.AddModelError("Name", "You cannot delete related data.");
                }
            }

            return RedirectToAction("Index");
        }
    }
}