﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ProAlert.Andon.Service.Interfaces;
using ProAlert.Andon.Service.Models;

namespace Vistech.Andon.Web.Controllers
{
    //[InitializeSimpleMembership]
    [Authorize(Roles = "Admin")]
    public class ProductController : Controller
    {
        private IRepository _repo;

        public ProductController(IRepository repo)
        {
            _repo = repo;
        }

        //
        // GET: /Product/

        public ActionResult Index()
        {
            //return View(db.Products.OrderBy(x => x.Name).ToList());
            return View(_repo.GetAll<Product>(orderBy: x => x.OrderBy(o => o.Name)));
        }

        //
        // GET: /Product/Details/5

        public ActionResult Details(int id = 0)
        {
            var product = _repo.GetById<Product>(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        //
        // GET: /Product/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Product/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="Id, Name, Description, Image, Material, StandardPack")] Product product, HttpPostedFileBase uploadFile)
        {
            if (ModelState.IsValid)
            {
                if (uploadFile != null)
                {
                    product.Image = new byte[uploadFile.ContentLength];
                    uploadFile.InputStream.Read(product.Image, 0, uploadFile.ContentLength);
                }
                _repo.Create(product, User.Identity.Name);
                _repo.Save();
                return RedirectToAction("Index");
            }

            return View(product);
        }

        //
        // GET: /Product/Edit/5

        public ActionResult Edit(int id = 0)
        {
            var product = _repo.GetById<Product>(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        //
        // POST: /Product/Edit/5

        [HttpPost]
        public ActionResult Edit([Bind(Include="Id, Description, Material, StandardPack")] Product product, HttpPostedFileBase uploadFile)
        {
            if (uploadFile != null && uploadFile.ContentLength > 0)
            {
                // extract only the fielname
                var fileName = Path.GetFileName(uploadFile.FileName);
                // store the file inside ~/App_Data/uploads folder
                if (fileName != null)
                {
                    var path = Path.Combine(HttpContext.Server.MapPath("~/Images/parts"), fileName);
                    uploadFile.SaveAs(path);
                }
            }
            var productToUpdate = _repo.GetById<Product>(product.Id);

            if (productToUpdate == null) return HttpNotFound();

            if (ModelState.IsValid)
            {
                if (uploadFile != null)
                {
                    productToUpdate.Image = new byte[uploadFile.ContentLength];
                    uploadFile.InputStream.Read(productToUpdate.Image, 0, uploadFile.ContentLength);
                }
                productToUpdate.Description = product.Description;
                productToUpdate.Material = product.Material;
                productToUpdate.StandardPack = product.StandardPack;

                _repo.Update(productToUpdate, User.Identity.Name);
                _repo.Save();
                return RedirectToAction("Index");
            }
            return View(product);
        }

        //
        // GET: /Product/Delete/5

        public ActionResult Delete(int id = 0)
        {
            var product = _repo.GetById<Product>(id);
            ViewBag.Msg = _repo.Get<ProductWorkCenterStat>(x => x.ProductID == id).Any() ? "Cannot delete." : "Are you sure you want to delete?";
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        //
        // POST: /Product/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                _repo.Delete<Product>(id);
                _repo.Save();
            }
            catch (Exception ex)
            {
                if (ex.Message.StartsWith("Violation of UNIQUE KEY constraint"))
                {
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    ModelState.AddModelError("Name", "You cannot delete related data.");
                }
            }
            return RedirectToAction("Index");
        }
    }
}