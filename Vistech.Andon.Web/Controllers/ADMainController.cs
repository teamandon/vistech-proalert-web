﻿using System.Web.Mvc;
using ProAlert.Andon.Service.Interfaces;
using ProAlert.Andon.Service.ViewModels;

namespace Vistech.Andon.Web.Controllers
{
    //[InitializeSimpleMembership]
    [Authorize(Roles = "AndonDisplay")]
    public class ADMainController : Controller
    {
        private IRepository _repo;

        public ADMainController(IRepository repo)
        {
            _repo = repo;
        }
        //
        // GET: /MainDisplay/

        public ActionResult ADMain()
        {
            var model = new AndonDisplayVM {IsForWc = false, Repo = _repo};
            return View(model.GetMessages);
        }

        public ActionResult UpdateDisplay()
        {
            var model = new AndonDisplayVM {IsForWc = false, Repo = _repo};
            return PartialView("_AndonDisplay", model.GetMessages);
        }
    }
}
