﻿using System.Web.Mvc;
using System.Web.Security;
using ProAlert.Andon.Service.Interfaces;


namespace Vistech.Andon.Web.Controllers
{
    public class RoleManagementController : Controller
    {
        private IRepository _repo;

        public RoleManagementController(IRepository repo)
        {
            _repo = repo;
        }

        //
        // GET: /RoleManagement/
        //[InitializeSimpleMembership]
        [Authorize(Roles = "Admin")]
        public ActionResult Index()
        {
            ViewBag.role = new SelectList(Roles.GetAllRoles());

            return View();
        }

        [Authorize(Roles = "Admin")]
        public ActionResult AddUserToRole(string user, string role)
        {
            if (!Roles.IsUserInRole(user, role))
                Roles.AddUserToRole(user, role);

            return View();
        }

        [Authorize(Roles = "Admin")]
        public ActionResult AddRole(string role)
        {
            if (!Roles.RoleExists(role))
                Roles.CreateRole(role);
            return View();
        }
    }
}
