﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Text;
using System.Transactions;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using ProAlert.Andon.Service.Interfaces;
using ProAlert.Andon.Service.Models;
using ProAlert.Andon.Service.ViewModels;
using Vistech.Andon.Web.Models;

namespace Vistech.Andon.Web.Controllers
{
    //[InitializeSimpleMembership]
    [Authorize(Roles = "Admin, Tech, Maint, Super")]
    public class ScrapController : Controller
    {
        #region < Members >
        protected readonly IRepository _repo;
        #endregion
        //
        // GET: /Scrap/
        public ScrapController(IRepository repo)
        {
            _repo = repo;
        }

        public ActionResult Index(bool? SignOff)
        {
            var scraps = _repo.Get<Scrap>(s => (SignOff == true ? s.SignoffDT != null : s.SignoffDT == null), orderBy: s => s.OrderByDescending(o => o.Id),
                includeProperties: "WorkCenter, LastStage, RejectCode, ScrapType");
            return View(scraps.ToList());
        }

        public ActionResult Filtered(int? WorkCenterID, int? ScrapTypeID, bool? SignOff, int? EmployeeID, bool? Parent)
        {
            PopulateAdminDD();
            PopulateScrapTypeDD();
            PopulateWorkCenterDD();
            List<Scrap> vm;
            if (WorkCenterID == null)
            {
                if (ScrapTypeID == null)
                {
                    vm = _repo.Get<Scrap>(s => (SignOff == true ? s.SignoffDT != null : s.SignoffDT == null) &&
                                                      ((EmployeeID == null
                                                           ? s.EmployeeID != null
                                                           : s.EmployeeID == EmployeeID) || s.EmployeeID == null) &&
                                                      (Parent == true ? s.Allocated : !s.Allocated)
                        , orderBy: s => s.OrderByDescending(o => o.Id)
                        , includeProperties: "WorkCenter, Product, LastStage, RejectCode, ScrapType").ToList();
                    return View(vm);
                }
                vm = _repo.Get<Scrap>(s => s.ScrapType.Id == ScrapTypeID && (SignOff == true ? s.SignoffDT != null : s.SignoffDT == null) &&
                    ((EmployeeID == null ? s.EmployeeID != null : s.EmployeeID == EmployeeID) || s.EmployeeID == null) && (Parent == true ? s.Allocated : !s.Allocated)
                    , orderBy: s => s.OrderByDescending(o => o.Id)
                    , includeProperties: "WorkCenter, Product, LastStage, RejectCode, ScrapType").ToList();
                return View(vm);
            }
            if (ScrapTypeID == null)
            {
                vm = _repo.Get<Scrap>(s => s.WorkCenterID == WorkCenterID && (SignOff == true ? s.SignoffDT != null : s.SignoffDT == null) &&
                        ((EmployeeID == null ? s.EmployeeID != null : s.EmployeeID == EmployeeID) || s.EmployeeID == null) && (Parent == true ? s.Allocated : !s.Allocated)
                                                         , orderBy: s => s.OrderByDescending(o => o.Id)
                                                         , includeProperties: "WorkCenter, Product, LastStage, RejectCode, ScrapType").ToList();
                return View(vm);
            }
            vm = _repo.Get<Scrap>(s => s.ScrapType.Id == ScrapTypeID && (SignOff == true ? s.SignoffDT != null : s.SignoffDT == null) &&
                s.WorkCenterID == WorkCenterID &&
                    ((EmployeeID == null ? s.EmployeeID != null : s.EmployeeID == EmployeeID) || s.EmployeeID == null) && (Parent == true ? s.Allocated : !s.Allocated)
                                                     , orderBy: s => s.OrderByDescending(o => o.Id)
                                                     , includeProperties: "WorkCenter, Product, LastStage, RejectCode, ScrapType").ToList();
            return View(vm);
        }
        //
        // GET: /Scrap/Details/5

        public ActionResult Details(int id = 0)
        {
            Scrap scrap = _repo.Get<Scrap>(x => x.Id == id, includeProperties: "WorkCenter, Product, LastStage, RejectCode, ScrapType").SingleOrDefault();
            if (scrap == null)
            {
                return HttpNotFound();
            }
            return View(scrap);
        }

        //
        // GET: /Scrap/Create

        public ActionResult Create()
        {
            var vm = new ScrapVM { 
                Scrap = new Scrap(),
                WorkCenters = _repo.Get<WorkCenter>(orderBy: s => s.OrderBy(o => o.Name)),
                Products = _repo.Get<Product>(orderBy: s => s.OrderBy(o => o.Name)),
                LastStages = _repo.Get<LastStage>(orderBy: s => s.OrderBy(o => o.Name)),
                RejectCodes = _repo.Get<RejectCode>(orderBy: s => s.OrderBy(o => o.Name)),
                ScrapTypes = _repo.Get<ScrapType>(orderBy: s => s.OrderBy(o => o.Name))
            };

            return View(vm);
        }

        private void PopulateWorkCenterDD(object selectedWorkCenter = null)
        {
            var workcenterQuery = from wc in _repo.Get<WorkCenter>()
                orderby wc.Name
                select wc;
            ViewBag.WorkCenterID = new SelectList(workcenterQuery, "Id", "Name", selectedWorkCenter);
        }

        private void PopulateScrapTypeDD(object selectedScrapType = null)
        {
            var scraptypeQuery = from s in _repo.Get<ScrapType>()
                orderby s.Name
                select s;
            ViewBag.ScrapTypeID = new SelectList(scraptypeQuery, "Id", "Name", selectedScrapType);
        }

        private void PopulateAdminDD(object selectedAdmin = null)
        {
            var list = new List<emp>();
            try
            {

                using (var aContext = new ApplicationDbContext())
                {
                    var userStore = new UserStore<ApplicationUser>(aContext);
                    var userManager = new UserManager<ApplicationUser>(userStore);
                    var lUsers = (from u in userManager.Users
                                  select u).ToList();
                    foreach (var user in lUsers)
                    {
                        if (!userManager.IsInRole(user.Id, "Admin") && !userManager.IsInRole(user.Id, "Super")) continue;
                        var e = _repo.GetFirst<Employee>(x => x.LogIn.Equals(user.UserName));
                        if (e == null) continue;
                        list.Add(new emp
                        {
                            EmployeeID = e.Id,
                            FullName = e.FullName
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                var ae = new AppError
                {
                    ErrorDT = DateTime.UtcNow,
                    ErrorMsg = ex.Message.ToString(),
                    Source = "ScrapController - PopulateAdminDD"
                };

                _repo.InsertError(ae);
                _repo.Save();
            }
            ViewBag.UserId = new SelectList(list, "EmployeeID", "FullName", selectedAdmin);
        }
        //
        // POST: /Scrap/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="ScrapTypeID, ScrapCount, WorkCenterID, ProductID, RejectCodeID, LastStageID, Notes, DefectLocation, ScrapParentID")] Scrap scrap, 
            [Bind(Include="Id, Qty")] List<ScrapType> scraptypes)
        {
            if (ModelState.IsValid)
            {
                var name = User.Identity.Name;
                var user = _repo.GetFirst <Employee>(x => x.LogIn.Trim() == name.Trim());
                int? empId = null;
                if (user != null)
                    empId = user.Id;
                scrap.ScrapTypeID = _repo.GetFirst<ScrapType>(s => s.Default).Id;

                scrap.ScrapDT = DateTime.UtcNow;
                scrap.EmployeeID = empId;

                using (var trans = new TransactionScope())
                {
                    // create parent scrap record
                    // get parentID
                    _repo.Create(scrap, name);
                    _repo.Save();
                    var id = scrap.Id;

                    foreach (var item in scraptypes.Where(x => x.Qty != null || x.Qty > 0))
                    {
                        var child = new Scrap
                        {
                            WorkCenterID = scrap.WorkCenterID,
                            ProductID = scrap.ProductID,
                            LastStageID = scrap.LastStageID,
                            RejectCodeID = scrap.RejectCodeID,
                            ScrapTypeID = item.Id,
                            ScrapCount = item.Qty ?? 0,
                            ScrapDT = scrap.ScrapDT,
                            EmployeeID = scrap.EmployeeID,
                            DefectLocation = scrap.DefectLocation,
                            ScrapParentID = id
                        };
                        _repo.Create(child, name);
                        _repo.Save();
                    }
                    trans.Complete();
                }

                return RedirectToAction("Filtered", new { WorkCenterID = scrap.WorkCenterID });
            }
            var vm = new ScrapVM
            {
                Scrap = scrap,
                WorkCenters = _repo.GetAll<WorkCenter>(orderBy: s => s.OrderBy(o => o.Name)),
                Products = _repo.GetAll<Product>(orderBy: s => s.OrderBy(o => o.Name)),
                LastStages = _repo.GetAll<LastStage>(orderBy: s => s.OrderBy(o => o.Name)),
                RejectCodes = _repo.GetAll<RejectCode>(orderBy: s => s.OrderBy(o => o.Name)),
                ScrapTypes = _repo.GetAll<ScrapType>(orderBy: s => s.OrderBy(o => o.Name))
            };
            return View(vm);
        }

        //
        // GET: /Scrap/Edit/5

        public ActionResult Edit(int id = 0)
        {
            var scrap = _repo.GetById<Scrap>(id);
            if (scrap == null)
            {
                return HttpNotFound();
            }

            var name = User.Identity.Name;
            var user = _repo.GetFirst<Employee>(x => x.LogIn.Trim() == name.Trim());
            if (user != null)
                scrap.EmployeeID = user.Id;

            var vm = new ScrapVM
            {
                Scrap = scrap,
                WorkCenters = _repo.GetAll<WorkCenter>(orderBy: s => s.OrderBy(o => o.Name)),
                Products = _repo.GetAll<Product>(orderBy: s => s.OrderBy(o => o.Name)),
                LastStages = _repo.GetAll<LastStage>(orderBy: s => s.OrderBy(o => o.Name)),
                RejectCodes = _repo.GetAll<RejectCode>(orderBy: s => s.OrderBy(o => o.Name)),
                ScrapTypes = _repo.GetAll<ScrapType>(orderBy: s => s.OrderBy(o => o.Name))
            };
            return View(vm);
        }

        //
        // POST: /Scrap/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id, ScrapTypeID, ScrapCount, WorkCenterID, ProductID, RejectCodeID, LastStageID, Notes, DefectLocation, TimesegmentID, ScrapDT, ScrapParentID, SignoffDT")] Scrap scrap,
            [Bind(Include = "Id, Qty")] List<ScrapType> scraptypes)
        {
            if (ModelState.IsValid)
            {
                var now = DateTime.UtcNow;
                var name = User.Identity.Name;
                var user = _repo.GetFirst<Employee>(x => x.LogIn.Trim() == name.Trim());
                int? empId = null;
                if (user != null)
                    empId = user.Id;

                var scrapToUpdate = _repo.GetById<Scrap>(scrap.Id);
                scrapToUpdate.EmployeeID = empId;
                scrapToUpdate.SignoffDT = now;
                if (scraptypes != null)
                {
                    foreach (var item in scraptypes.Where(x => x.Qty != null || x.Qty > 0))
                    {
                        var child = new Scrap
                        {
                            WorkCenterID = scrap.WorkCenterID,
                            ProductID = scrap.ProductID,
                            LastStageID = scrap.LastStageID,
                            RejectCodeID = scrap.RejectCodeID,
                            ScrapTypeID = item.Id,
                            ScrapCount = item.Qty ?? 0,
                            ScrapDT = scrap.ScrapDT,
                            EmployeeID = empId,
                            DefectLocation = scrap.DefectLocation,
                            ScrapParentID = scrap.Id,
                            TimesegmentID = scrap.TimesegmentID,
                            Notes = scrap.Notes,
                            SignoffDT = now
                        };
                        _repo.Create(child, User.Identity.Name);
                        //_repo.Save();
                        scrapToUpdate.Allocated = true;
                    }
                }
                scrapToUpdate.DefectLocation = scrap.DefectLocation;
                scrapToUpdate.Notes = scrap.Notes;
                scrapToUpdate.LastStageID = scrap.LastStageID;
                scrapToUpdate.RejectCodeID = scrap.RejectCodeID;
                _repo.Update(scrapToUpdate, User.Identity.Name);

                try
                {
                    _repo.Save();
                }
                catch (DbEntityValidationException ex)
                {
                    _repo.InsertError(new AppError { ErrorDT = DateTime.UtcNow, ErrorMsg = "WC: " + scrap.WorkCenterID + " -- " + ex.Message, Source = "ScrapController - Edit" });
                    _repo.Save();
                }
                return RedirectToAction("Filtered", new { WorkCenterID = scrap.WorkCenterID });
            }
            var vm = new ScrapVM
            {
                Scrap = scrap,
                WorkCenters = _repo.GetAll<WorkCenter>(orderBy: s => s.OrderBy(o => o.Name)),
                Products = _repo.GetAll<Product>(orderBy: s => s.OrderBy(o => o.Name)),
                LastStages = _repo.GetAll<LastStage>(orderBy: s => s.OrderBy(o => o.Name)),
                RejectCodes = _repo.GetAll<RejectCode>(orderBy: s => s.OrderBy(o => o.Name)),
                ScrapTypes = _repo.GetAll<ScrapType>(orderBy: s => s.OrderBy(o => o.Name))
            };
            return View(vm);
        }
        //public ActionResult Edit([Bind(Include = "ScrapID, WorkCenterID, ProductID, LastStageID, RejectCodeID, ScrapTypeID, ScrapCount, DefectLocation, EmployeeID, Notes")] Scrap scrap)
        //{
        //    var scrapToUpdate =
        //        db.ScrapRepository.Get(x => x.ScrapID == scrap.ScrapID,
        //            includeProperties: "WorkCenter, Product, LastStage, RejectCode, ScrapType").Single();

        //    if (ModelState.IsValid)
        //    {
        //        scrapToUpdate.WorkCenterID = scrap.WorkCenterID;
        //        scrapToUpdate.ProductID = scrap.ProductID;
        //        scrapToUpdate.RejectCodeID = scrap.RejectCodeID;
        //        scrapToUpdate.LastStageID = scrap.LastStageID;
        //        scrapToUpdate.ScrapTypeID = scrap.ScrapTypeID;
        //        scrapToUpdate.DefectLocation = scrap.DefectLocation;
        //        scrapToUpdate.ScrapCount = scrap.ScrapCount;
        //        scrapToUpdate.SignoffDT = DateTime.Now;
        //        scrapToUpdate.EmployeeID = scrap.EmployeeID;
        //        scrapToUpdate.Notes = scrap.Notes;

        //        db.ScrapRepository.Update(scrapToUpdate);
        //        db.Save();
        //        return RedirectToAction("Filtered", new { WorkCenterID = scrap.WorkCenterID });
        //    }
        //    var vm = new ScrapVM
        //    {
        //        Scrap = scrap,
        //        WorkCenters = db.WorkCenterRepository.Get(orderBy: s => s.OrderBy(o => o.Name)),
        //        Products = db.ProductRepository.Get(orderBy: s => s.OrderBy(o => o.Name)),
        //        LastStages = db.LastStageRepository.Get(orderBy: s => s.OrderBy(o => o.Name)),
        //        RejectCodes = db.RejectCodeRepository.Get(orderBy: s => s.OrderBy(o => o.Name)),
        //        ScrapTypes = db.ScrapTypeRepository.Get(orderBy: s => s.OrderBy(o => o.Name))
        //    };

        //    return View(vm);
        //}

        //
        // GET: /Scrap/Delete/5

        public ActionResult Delete(int id = 0)
        {
            var scrap = _repo.GetFirst<Scrap>(x => x.Id == id, includeProperties: "WorkCenter, Product, LastStage, RejectCode, ScrapType");
            if (scrap == null)
            {
                return HttpNotFound();
            }
            return View(scrap);
        }

        //
        // POST: /Scrap/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var scrap = _repo.GetById<Scrap>(id);
            try
            {
                _repo.Delete<Scrap>(id);
                _repo.Save();
            }
            catch (Exception ex)
            {
                if (ex.Message.StartsWith("Violation of UNIQUE KEY constraint"))
                {
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    ModelState.AddModelError("Name", "You cannot delete related data.");
                }
            }

            return RedirectToAction("Filtered", new { WorkCenterID = scrap.WorkCenterID });
        }

        public ActionResult ScrapTypeCreate()
        {
            if (Request != null && Request.IsAjaxRequest())
                return PartialView("_ScrapTypeCreate", new ScrapType());
            return View("_ScrapTypeCreate", new ScrapType());
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult ScrapTypeCreate(ScrapType model)
        {
            var statusCode = 0;
            var errorMsg = string.Empty;
            StringBuilder errorMessage = null;
            if(model.Name == null)
                ModelState.AddModelError("Name", "Name has to be filled");
            if(_repo.Get<ScrapType>(x => x.Name.Equals(model.Name)).Any())
                ModelState.AddModelError("Name", "Already exists");
            if (!ModelState.IsValid)
            {
                if (Request.IsAjaxRequest())
                {
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    return PartialView("_ScrapTypeCreate", model);
                }
                return PartialView("_ScrapTypeCreate", model);
            }
            try
            {
                _repo.Create(model, User.Identity.Name);
                _repo.Save();
            }
            catch (Exception exp)
            {
                errorMessage = new StringBuilder(200);
                errorMessage.AppendFormat(
                    "<div class=\"validation-summary-errors\" title\"Server Error\">{0}</div>",
                    exp.GetBaseException().Message);
                statusCode = (int)HttpStatusCode.InternalServerError; // = 500
            }
            if (Request.IsAjaxRequest())
            {
                if (statusCode > 0)
                {
                    Response.StatusCode = statusCode;
                    return Content(errorMessage.ToString());
                }
                TempData["message"] = "Scrap Type was added.";
                var id = _repo.Get<ScrapType>().Last().Id;
                return Json(new { sl = new SelectList(_repo.GetAll<ScrapType>(), "Id", "Name"), idx = id} , JsonRequestBehavior.AllowGet);
            }
            return RedirectToAction("Create");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FinishLater([Bind(Include = "Id, ScrapTypeID, ScrapCount, WorkCenterID, ProductID, RejectCodeID, LastStageID, Notes, DefectLocation, TimesegmentID, ScrapDT, ScrapParentID, SignoffDT")] Scrap scrap,
            [Bind(Include = "Id, Qty")] List<ScrapType> scraptypes)
        {
            if (ModelState.IsValid)
            {
                var name = User.Identity.Name;
                var user = _repo.GetFirst<Employee>(x => x.LogIn.Trim() == name.Trim());
                int? empId = null;
                if (user != null)
                    empId = user.Id;

                var scrapToUpdate = _repo.GetById<Scrap>(scrap.Id);
                scrapToUpdate.EmployeeID = empId;

                if (scraptypes != null)
                {
                    foreach (var item in scraptypes.Where(x => x.Qty != null || x.Qty > 0))
                    {
                        var child = new Scrap
                        {
                            WorkCenterID = scrap.WorkCenterID,
                            ProductID = scrap.ProductID,
                            LastStageID = scrap.LastStageID,
                            RejectCodeID = scrap.RejectCodeID,
                            ScrapTypeID = item.Id,
                            ScrapCount = item.Qty ?? 0,
                            ScrapDT = scrap.ScrapDT,
                            EmployeeID = empId,
                            DefectLocation = scrap.DefectLocation,
                            ScrapParentID = scrap.Id,
                            TimesegmentID = scrap.TimesegmentID,
                            Notes = scrap.Notes
                        };
                        _repo.Create(child, User.Identity.Name);
                        //_repo.Save();
                        scrapToUpdate.Allocated = true;
                    }
                }
                scrapToUpdate.DefectLocation = scrap.DefectLocation;
                scrapToUpdate.Notes = scrap.Notes;
                scrapToUpdate.LastStageID = scrap.LastStageID;
                scrapToUpdate.RejectCodeID = scrap.RejectCodeID;
                _repo.Update(scrapToUpdate, User.Identity.Name);

                try
                {
                    _repo.Save();
                }
                catch (DbEntityValidationException ex)
                {
                    _repo.InsertError(new AppError { ErrorDT = DateTime.UtcNow, ErrorMsg = "WC: " +  scrap.WorkCenterID + " -- " + ex.Message, Source = "ScrapController - Edit"});
                    _repo.Save();
                }
                return RedirectToAction("Filtered", new { WorkCenterID = scrap.WorkCenterID });
            }
            var vm = new ScrapVM
            {
                Scrap = scrap,
                WorkCenters = _repo.GetAll<WorkCenter>(orderBy: s => s.OrderBy(o => o.Name)),
                Products = _repo.GetAll<Product>(orderBy: s => s.OrderBy(o => o.Name)),
                LastStages = _repo.GetAll<LastStage>(orderBy: s => s.OrderBy(o => o.Name)),
                RejectCodes = _repo.GetAll<RejectCode>(orderBy: s => s.OrderBy(o => o.Name)),
                ScrapTypes = _repo.GetAll<ScrapType>(orderBy: s => s.OrderBy(o => o.Name))
            };
            return View("Edit", vm);
        }
        //public ActionResult FinishLater([Bind(Include = "ScrapID, WorkCenterID, ProductID, LastStageID, RejectCodeID, ScrapTypeID, ScrapCount, DefectLocation, EmployeeID, Notes")] Scrap scrap)
        //{
        //    var scrapToUpdate =
        //        db.ScrapRepository.Get(x => x.ScrapID == scrap.ScrapID,
        //            includeProperties: "WorkCenter, Product, LastStage, RejectCode, ScrapType").Single();

        //    if (ModelState.IsValid)
        //    {
        //        scrapToUpdate.WorkCenterID = scrap.WorkCenterID;
        //        scrapToUpdate.ProductID = scrap.ProductID;
        //        scrapToUpdate.RejectCodeID = scrap.RejectCodeID;
        //        scrapToUpdate.LastStageID = scrap.LastStageID;
        //        scrapToUpdate.ScrapTypeID = scrap.ScrapTypeID;
        //        scrapToUpdate.DefectLocation = scrap.DefectLocation;
        //        scrapToUpdate.EmployeeID = scrap.EmployeeID;
        //        scrapToUpdate.ScrapCount = scrap.ScrapCount;
        //        scrapToUpdate.Notes = scrap.Notes;

        //        db.ScrapRepository.Update(scrapToUpdate);
        //        db.Save();
        //        return RedirectToAction("Filtered", new { WorkCenterID = scrap.WorkCenterID });
        //    }
        //    var vm = new ScrapVM
        //    {
        //        Scrap = scrap,
        //        WorkCenters = db.WorkCenterRepository.Get(orderBy: s => s.OrderBy(o => o.Name)),
        //        Products = db.ProductRepository.Get(orderBy: s => s.OrderBy(o => o.Name)),
        //        LastStages = db.LastStageRepository.Get(orderBy: s => s.OrderBy(o => o.Name)),
        //        RejectCodes = db.RejectCodeRepository.Get(orderBy: s => s.OrderBy(o => o.Name)),
        //        ScrapTypes = db.ScrapTypeRepository.Get(orderBy: s => s.OrderBy(o => o.Name))
        //    };

        //    return View("Edit", vm);
        //}

        public ActionResult LoadImage(int prodid)
        {
            var image = _repo.GetById<Product>(prodid);
            if (image.Image == null)
            {
                var path = Server.MapPath(Url.Content(@"~\parts\NoImage.png"));
                var defaultImg = System.IO.File.ReadAllBytes(path);
                var base64 = Convert.ToBase64String(defaultImg);
                var noimageSrc = string.Format("data:image/png;base64,{0}", base64);
                return Json(new { img = noimageSrc }, JsonRequestBehavior.AllowGet);
            }
            var imageBase64 =  Convert.ToBase64String(image.Image);
            var imageSrc = string.Format("data:image/gif;base64,{0}", imageBase64);
            return Json(new {img = imageSrc}, JsonRequestBehavior.AllowGet);
        }

    }

    public class emp
    {
        public int EmployeeID { get; set; }
        public string FullName { get; set; }
    }
}