﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using ProAlert.Andon.Service.Interfaces;
using ProAlert.Andon.Service.Models;

namespace Vistech.Andon.Web.Controllers
{
    [Authorize(Roles = "Admin")]
    public class SubCategoriesController : Controller
    {
        #region < Members >
        private readonly IRepository _repo;
        #endregion

        public SubCategoriesController(IRepository repo)
        {
            _repo = repo;
        }
        // GET: SubCategories
        public ActionResult Index()
        {
            return View(_repo.Get<SubCategory>().ToList());
        }

        // GET: SubCategories/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var subCategory = _repo.GetById<SubCategory>(id);
            if (subCategory == null)
            {
                return HttpNotFound();
            }
            return View(subCategory);
        }

        // GET: SubCategories/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: SubCategories/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Name,Description")] SubCategory subCategory)
        {
            if (_repo.Get<SubCategory>(x => x.Name.Equals(subCategory.Name)).Any())
                ModelState.AddModelError("Name", "Already exists.");
            if (ModelState.IsValid)
            {
                _repo.Create(subCategory, User.Identity.Name);
                _repo.Save();
                return RedirectToAction("Index");
            }

            return View(subCategory);
        }

        // GET: SubCategories/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var subCategory = _repo.GetById<SubCategory>(id);
            if (subCategory == null)
            {
                return HttpNotFound();
            }
            return View(subCategory);
        }

        // POST: SubCategories/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Description")] SubCategory subCategory)
        {
            if (ModelState.IsValid)
            {
                var rowToUpdate = _repo.GetById<SubCategory>(subCategory.Id);
                rowToUpdate.Description = subCategory.Description;
                _repo.Update(rowToUpdate, User.Identity.Name);
                _repo.Save();
                return RedirectToAction("Index");
            }
            return View(subCategory);
        }

        // GET: SubCategories/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ViewBag.Msg = _repo.Get<CallLog>(x => x.SubCategoryID == id).Any() ? "Cannot delete." : "Are you sure you want to delete?";
            var subCategory = _repo.GetById<SubCategory>(id);
            if (subCategory == null)
            {
                return HttpNotFound();
            }
            return View(subCategory);
        }

        // POST: SubCategories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var subCategory = _repo.GetById<SubCategory>(id);
            _repo.Delete(subCategory);
            _repo.Save();
            return RedirectToAction("Index");
        }
    }
}
