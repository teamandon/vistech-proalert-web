﻿using System;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using ProAlert.Andon.Service.Interfaces;
using ProAlert.Andon.Service.Models;
using ProAlert.Andon.Service.ViewModels;
using Vistech.Andon.Web.Tools;
using timeconversions = ProAlert.Andon.Service.Common.timeconversions;

namespace Vistech.Andon.Web.Controllers
{
    //[InitializeSimpleMembership]
    [Authorize(Roles = "Admin, Super")]
    public class DailyController : Controller
    {
        #region < Members >
        private IRepository _repo;
        #endregion
        public DailyController(IRepository repo)
        {
            _repo = repo;
        }
        public ActionResult Daily()
        {
            var userDt = HttpContext.Session["drillDownDt"] as DateTime?;
            var summaryStart = userDt ?? DateTime.UtcNow.AddHours(-24);


            var yesterday = summaryStart;
            ViewBag.userDt = yesterday;
            var obj = new TlRpt();
            var tlList = obj.GetTls(_repo.Get<WorkCenter>().ToList(), yesterday, DateTime.UtcNow);
            foreach (var tl in tlList)
            {
                tl.CacheID = tl.TL.Id.ToString();
            }

            return View(tlList);
        }

        public ActionResult DisplayOEEStartTime()
        {
            var tzi = (TimeZoneInfo)Session["tzi"];
            if (tzi == null)
            {
                var dnow = DateTime.Now;
                ViewBag.Date = dnow.ToShortDateString();
                ViewBag.Time = dnow.Hour.ToString().PadLeft(2, '0') + dnow.Minute.ToString().PadLeft(2, '0');
                return PartialView("_DateTime");
            }

            var userDt = HttpContext.Session["drillDownDt"] as DateTime?;
            var summaryStart = userDt ?? DateTime.UtcNow.AddHours(-24);

            var now = timeconversions.UTCtoClient(tzi, summaryStart);

            ViewBag.Date = now.ToShortDateString();
            ViewBag.Time = now.Hour.ToString().PadLeft(2, '0') + now.Minute.ToString().PadLeft(2, '0');

            return PartialView("_DateTime");
        }

        public ActionResult SetOEEStart(string date, string time)
        {
            var tzi = (TimeZoneInfo)Session["tzi"];
            var startdate = DateTime.Parse(date);

            var dbStartDate = timeconversions.ClientToUTC(tzi, startdate, time);

            if (dbStartDate > DateTime.UtcNow)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return PartialView("_DateTime");
            }
            HttpContext.Session["drillDownDt"] = dbStartDate;

            return Json(startdate.ToString("M/dd/yyyy HH:mm:ss"), JsonRequestBehavior.AllowGet);
        }

        public ActionResult DisplayTimeLine(string id)
        {
            var tl = string.Empty;
            var sctl = string.Empty;
            ProductTimelineStatsVM oeeHeader;

            //using (var context = new ProAlertContext())
            //{
            //    context.Configuration.ProxyCreationEnabled = false;
            //    using (var repo = new EntityFrameworkRepository<ProAlertContext>(context))
            //    {
            var userDt = HttpContext.Session["drillDownDt"] as DateTime?;
            var summaryStart = userDt ?? DateTime.UtcNow.AddHours(-24);
            var obj = new TlRpt();
            oeeHeader = obj.GetTl(int.Parse(id), summaryStart, DateTime.UtcNow);
            //    }
            //    context.Configuration.ProxyCreationEnabled = false;
            //}
            if (oeeHeader == null) return Json(tl, JsonRequestBehavior.AllowGet);
            var now = DateTime.UtcNow;
            //var stats = (StatsVM) Session[wcproduct];
            if (oeeHeader.TL.Stop != null)
                now = oeeHeader.TL.Stop ?? now;

            double totTime = now.Subtract(oeeHeader.Start).TotalMilliseconds;

            double ratio = 1350 / totTime;
            tl += "<div id='"+ id +"' class='tlLeft' style='margin-top: 10px; float: left;'></div>";
            sctl += "<div class='sLeft'></div>";
            foreach (var dr in oeeHeader.TL.Timesegments.OrderBy(x => x.Start))
            {
                var tsStop = dr.Stop ?? oeeHeader.Stop;
                var tsStart = dr.Start < oeeHeader.Start ? oeeHeader.Start : dr.Start;
                double len = tsStop.Subtract(tsStart).TotalMilliseconds; //dr.Start < oeeHeader.Start ? oeeHeader.Start : dr.Start
                len = len * ratio;
                len = len < 1 ? 3 : len;
                var color = "runColor";
                var dtReason = string.Empty;
                var scrapDiv = string.Empty;
                if (dr.UnplannedDt == true)
                    color = "dtColor";
                if (dr.UnplannedDt == true && dr.PlannedDt == true)
                    color = "pdtanddtColor";
                if (dr.PlannedDt == true && !dr.UnplannedDt == true)
                    color = "pdtColor";

                var timeframe = oeeHeader.WCProduct + "&#013;" +
                    timeconversions.UTCtoClient((TimeZoneInfo)Session["tzi"], tsStart).ToString("M/dd/yyyy HH:mm:ss") + " -  &#013;" +
                    timeconversions.UTCtoClient((TimeZoneInfo)Session["tzi"], tsStop).ToString("M/dd/yyyy HH:mm:ss");

                if (dr.CallLogs != null)
                {
                    foreach (var c in dr.CallLogs)
                    {
                        var callName = c.Call.Name;
                        var endTime = dr.UnplannedDt == true ? c.ResolveDt ?? tsStop : c.ResponseDt ?? tsStop;
                        var minutes = endTime.Subtract(c.InitiateDt).TotalMilliseconds / 60000;

                        dtReason += "<b>" + callName + "</b> ";

                        var call = " &#013;" + callName + " : " + minutes.ToString("N1") + "&#013;   Initiated:" + timeconversions.UTCtoClient((TimeZoneInfo)Session["tzi"], c.InitiateDt).ToString("M/dd/yyyy HH:mm:ss")
                            + " &#013;   Respond: " + (c.ResponseDt == null ? " " : timeconversions.UTCtoClient((TimeZoneInfo)Session["tzi"], c.ResponseDt ?? now).ToString("M/dd/yyyy HH:mm:ss"))
                            + (c.Call.DT ? " &#013;   Resolved: " + (c.ResolveDt == null ? " " : timeconversions.UTCtoClient((TimeZoneInfo)Session["tzi"], c.ResolveDt ?? now).ToString("M/dd/yyyy HH:mm:ss")) : "");
                        timeframe += call;
                    }
                }

                if (dr.PlannedDt == true)
                {
                    foreach (var p in dr.PlannedDtLogs)
                    {
                        var callName = p.PlannedDt.Name;
                        var endTime = p.Stop ?? tsStop;
                        var minutes = endTime.Subtract(p.Start).TotalMilliseconds / 60000;
                        dtReason += callName + " ";
                        var planned = " &#013;" + callName + " : " + minutes.ToString("N1") + "&#013;   Start: " + timeconversions.UTCtoClient((TimeZoneInfo)Session["tzi"], p.Start).ToString("M/dd/yyyy HH:mm:ss")
                            + " &#013;   Stop: " + (p.Stop == null ? " " : timeconversions.UTCtoClient((TimeZoneInfo)Session["tzi"], p.Stop ?? now).ToString("M/dd/yyyy HH:mm:ss"));
                        timeframe += planned;
                    }
                }
                if (dr.Scraps != null)
                {
                    var first = tsStart;
                    var scrapCnt = dr.Scraps.Count();
                    foreach (var s in dr.Scraps.Where(x => !x.Allocated))
                    {
                        double dis = s.ScrapDT.Subtract(first).TotalMilliseconds * ratio;
                        first = s.ScrapDT;
                        dis = dis - (8 * scrapCnt) < 0 ? 0 : dis - (8 * scrapCnt);
                        scrapDiv += "<div style='height: 10px; float: left; color: red; padding-left: " + (int)dis + "px;'>^</div>";
                        scrapCnt--;
                    }
                    var cnt = dr.Scraps.Sum(s => s.ScrapCount);
                    if (cnt > 0)
                    {
                        timeframe += "&#013; Scrap: " + cnt;
                        color = color == "runColor" ? "runScrap" : color;
                    }
                }
                timeframe += "&#013; Cycles: " + (dr.CycleSummary?.Count ?? 0);


                if (dtReason.Length == 0)
                    dtReason = " ";
                tl += "<div id='" + dr.Id + "' class='" + color + "' title='" + timeframe + "' style='margin-top:10px ; float: left; width: " + (int)len + "px;'>" + (len > 100 ? dtReason : "") + "</div>";
                sctl += "<div style='height: 10px; float: left; width: " + (int)len + "px;'>" + scrapDiv + "</div>";
            }
            sctl += "<div class='sRight'></div>";
            tl += "<div class='tlRight' style='width: 5px; margin-top: 10px; float: left'></div><p style='clear:both; margin:0;'></p>" + sctl;
            return Json(tl, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetTSDetails(int? tlid, int? tsid)
        {
            ProductTimelineStatsVM oeeHeader;

            var userDt = HttpContext.Session["drillDownDt"] as DateTime?;

            using (var context = new ProAlertContext())
            {
                context.Configuration.ProxyCreationEnabled = false;
                using (var repo = new EntityFrameworkRepository<ProAlertContext>(context))
                {
                    var summaryStart = userDt ?? DateTime.UtcNow.AddHours(-24);
                    var obj = new TlRpt();
                    oeeHeader = obj.GetTl(tlid ?? 0, summaryStart, DateTime.UtcNow);
                    oeeHeader.CacheID = tlid.ToString(); //    }
                    context.Configuration.ProxyCreationEnabled = false;
                    ViewBag.userDt = summaryStart;
                }
            }

            if (tsid == null) return HttpNotFound();

            // populate the times segment completely.
            var ts = oeeHeader.TL.Timesegments.Single(x => x.Id == tsid);

            if (ts == null) return HttpNotFound();


            return PartialView("_TSDetailsContainer", ts);
        }
    }
}
