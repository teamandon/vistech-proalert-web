﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;
using Microsoft.AspNet.SignalR.Hubs;
using ProAlert.Andon.Service.Common;
using ProAlert.Andon.Service.Interfaces;
using ProAlert.Andon.Service.Models;
using ProAlert.Andon.Service.ViewModels;

namespace Vistech.Andon.Web
{
    public class AndonDisplay : IAndonDisplay
    {
        private readonly AndonDisplayMsgSet _msgSet;
        private readonly AdvisoryMsgSet _advisoryMsgSet;
        private readonly Timer _timer;
        private readonly object _updateAndonDisplayLock = new object();
        private volatile bool _updatingAndonDisplay = false;
        private AndonDisplayMsgSet _lastMsg;
        private AdvisoryMsgSet _lastAdvisoryMsgSet;
        public AndonDisplay(IHubConnectionContext<dynamic> clients)
        {
            _lastMsg = new AndonDisplayMsgSet();
            Clients = clients;
            using (var context = new ProAlertContext())
            {
                using (var repo = new EfReadOnlyRepository.EntityFrameworkReadOnlyRepository<ProAlertContext>(context))
                {
                    var vm = new AndonDisplayVM { Repo = repo };
                    var nMsg = new AndonDisplayMsgSet { Maintenance = vm.Maintenance.message, Supervisor = vm.QC.message, Materials = vm.Materials.message };
                    var nAdMsg = new AdvisoryMsgSet {Advisory = vm.Advisory.message, Image = vm.Advisory.Image};
                    _msgSet = nMsg;
                    _advisoryMsgSet = nAdMsg;
                    _lastMsg = nMsg;
                    _lastAdvisoryMsgSet = nAdMsg;
                }
            }
            _timer = new Timer {AutoReset = false};
            _timer.Elapsed += (TimeElapsed);
            _timer.Start();
        }
        static double GetInterval()
        {
            var now = DateTime.Now;
            return ((60 - now.Second) * 1000 - now.Millisecond);
        }
        private void TimeElapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            var now = DateTime.Now;
            // check every ten seconds
            if (now.Second % 6 == 0)
            {
                using (var context = new ProAlertContext())
                {
                    if (!_updatingAndonDisplay)
                        InitiateCall(context, 0);
                }
            }
            _timer.Interval = GetInterval();
            _timer.Start();
        }
        private IHubConnectionContext<dynamic> Clients
        {
            get;
            set;
        }

        public AndonDisplayMsgSet GetMsgSet()
        {
            return _msgSet;
        }

        public AdvisoryMsgSet GetAdSet()
        {
            return _advisoryMsgSet;
        }


        public Task InitiateCall(ProAlertContext context, int wccid)
        {
            lock (_updateAndonDisplayLock)
            {
                if (!_updatingAndonDisplay)
                {
                    using (var repo = new EntityFrameworkRepository<ProAlertContext>(context))
                    {
                        try
                        {
                            _updatingAndonDisplay = true;

                            var call = repo.GetOne<WorkCenterCall>(x => x.Id == wccid, "Call, WorkCenter");
                            var vm = new AndonDisplayVM {Repo = repo};
                            var nMsg = new AndonDisplayMsgSet
                            {
                                Maintenance = vm.Maintenance.message,
                                Supervisor = vm.QC.message,
                                Materials = vm.Materials.message
                            };
                            var nAdMsg = new AdvisoryMsgSet {Advisory = vm.Advisory.message, Image = vm.Advisory.Image};
                            if (nAdMsg != _lastAdvisoryMsgSet)
                            {
                                _lastAdvisoryMsgSet = nAdMsg;
                                BroadcastAdvisory(nAdMsg);
                            }
                            if (call != null)
                            {
                                var dtMsg = "<label style='padding-left:1em;'  class='lblDT'>" + call.WorkCenter.Name +
                                            "-" + call.Call.Name + "(0)</label>";
                                var nondtMsg = "<label style='padding-left:1em;'  class='lblNonDT'>" +
                                               call.WorkCenter.Name + "-" + call.Call.Name + "(0)</label>";
                                switch (call.Call.Type)
                                {
                                    case Enumerations.CallType.Maintenance:
                                        if (nMsg.Maintenance.Contains("OK"))
                                            nMsg.Maintenance = string.Empty;
                                        if (!nMsg.Maintenance.Contains(nondtMsg) && !nMsg.Maintenance.Contains(dtMsg))
                                        {
                                            if (call.Call.DT)
                                                nMsg.Maintenance = dtMsg;
                                            else
                                                nMsg.Maintenance += nondtMsg;
                                        }
                                        break;
                                    case Enumerations.CallType.Materials:
                                        if (nMsg.Materials.Contains("OK"))
                                            nMsg.Materials = string.Empty;
                                        if (!nMsg.Materials.Contains(dtMsg) && !nMsg.Materials.Contains(nondtMsg))
                                        {
                                            if (call.Call.DT)
                                                nMsg.Materials = dtMsg;
                                            else
                                                nMsg.Materials += nondtMsg;
                                        }
                                        break;
                                    case Enumerations.CallType.QC:
                                        if (nMsg.Supervisor.Contains("OK"))
                                            nMsg.Supervisor = string.Empty;
                                        if (!nMsg.Supervisor.Contains(dtMsg) && !nMsg.Supervisor.Contains(nondtMsg))
                                        {
                                            if (call.Call.DT)
                                                nMsg.Supervisor = dtMsg;
                                            else
                                                nMsg.Supervisor += nondtMsg;
                                        }
                                        break;
                                }
                                //  Sound Engine listens for callInitiate()
                                Clients.All.callInitiate(wccid);
                            }
                            if (nMsg.Materials.Equals(_lastMsg.Materials) &&
                                nMsg.Maintenance.Equals(_lastMsg.Maintenance) &&
                                nMsg.Supervisor.Equals(_lastMsg.Supervisor)) return null;
                            _lastMsg = nMsg;
                            BroadcastAndonDisplayMsg(nMsg);
                        }
                        catch (Exception ex)
                        {
                            repo.InsertError(new AppError
                            {
                                ErrorDT = DateTime.UtcNow,
                                ErrorMsg = ex.Message,
                                Source = "AndonDisplayHub"
                            });
                        }
                        finally
                        {
                            _updatingAndonDisplay = false;
                        }
                    }
                }
            }
            return null;
        }

        public void CustomMsg(string advisory)
        {
            lock (_updateAndonDisplayLock)
            {
                if (!_updatingAndonDisplay)
                {
                    _updatingAndonDisplay = true;
                    var nMsg = new AdvisoryMsgSet { Advisory = advisory };
                    _updatingAndonDisplay = false;
                    BroadcastAdvisory(nMsg);
                }
            }
        }

        private void BroadcastAdvisory(AdvisoryMsgSet msgSet)
        {
            // for Andon Screen
            Clients.All.updateAdvisory(msgSet);
            // remove image for WCs
            msgSet.Image = null;
            Clients.All.updateWcAdvisory(msgSet);
        }
        private void BroadcastAndonDisplayMsg(AndonDisplayMsgSet msgSet)
        {
            Clients.All.updateAndonDisplay(msgSet);
        }
    }
}