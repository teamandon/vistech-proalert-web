﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Vistech.Andon.Web.helpers
{
    public class EmailTools
    {
        /// <summary>
        /// convert list to datatable
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data"></param>
        /// <returns></returns>
        public static DataTable ConvertToDataTable<T>(IList<T> data)
        {
            var properties = TypeDescriptor.GetProperties(typeof(T));
            var table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                var row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="Body"></param>
        /// <param name="bTime"></param>
        /// <returns></returns>
        public static string AddHTMLTableToEmailBody(DataTable dt, string Body, bool bTime = false)
        {
            var rowIndex = 0;

            Body += "<br/><table style='width: 100%; font-family:arial; font-size:15px;' ><tr>";
            Body = dt.Columns.Cast<DataColumn>().Aggregate(Body, (current, dc) => current + ("<th>" + dc.ColumnName + "</th>"));
            Body += "</tr>";
            foreach (DataRow dr in dt.Rows)
            {
                rowIndex += 1;
                if (rowIndex % 2 > 0)
                    Body += "<tr style='color:#333333;background-color:Aqua;'>";
                else
                    Body += "<tr style='color:#284775;background-color:White;'>";
                foreach (DataColumn dc in dt.Columns)
                {
                    if (IsDate(dr[dc.ColumnName]))
                    {
                        var dDate = DateTime.Parse(dr[dc.ColumnName].ToString());
                        Body += "<td>" + (bTime ? dDate.ToString("M/dd/yyyy HH:mm:ss") : dDate.ToString("d"));
                    }
                    else
                        Body += "<td>" + dr[dc.ColumnName].ToString() + "</td>";
                }
                Body += "</tr>";
            }
            return Body + "</table>";
        }

        public static bool IsDate(object Expression)
        {
            if (Expression != null)
            {
                if (Expression is DateTime)
                    return true;
                if (Expression is string)
                {
                    DateTime time1;
                    return DateTime.TryParse((string)Expression, out time1);
                }
            }
            return false;
        }
    }

    public static class bstExtensions
    {
        public static IEnumerable<TSource> DistinctBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
        {
            var seenKeys = new HashSet<TKey>();
            return source.Where(element => seenKeys.Add(keySelector(element)));
        }
    }
    public static class ValidationMessageExtensions
    {

        public static IHtmlString MyValidationSummary(this HtmlHelper htmlHelper)
        {
            var formContextForClientValidation = htmlHelper.ViewContext.ClientValidationEnabled ? htmlHelper.ViewContext.FormContext : null;
            if (htmlHelper.ViewData.ModelState.IsValid)
            {
                if (formContextForClientValidation == null)
                {
                    return null;
                }
                if (htmlHelper.ViewContext.UnobtrusiveJavaScriptEnabled)
                {
                    return null;
                }
            }

            var stringBuilder = new StringBuilder();
            var ulBuilder = new TagBuilder("ul");

            ModelState modelState;
            if (htmlHelper.ViewData.ModelState.TryGetValue(htmlHelper.ViewData.TemplateInfo.HtmlFieldPrefix, out modelState))
            {
                foreach (ModelError error in modelState.Errors)
                {
                    string userErrorMessageOrDefault = error.ErrorMessage;
                    if (!string.IsNullOrEmpty(userErrorMessageOrDefault))
                    {
                        var liBuilder = new TagBuilder("li") {InnerHtml = userErrorMessageOrDefault};
                        stringBuilder.AppendLine(liBuilder.ToString(TagRenderMode.Normal));
                    }
                }
            }

            if (stringBuilder.Length == 0)
            {
                stringBuilder.AppendLine("<li style=\"display:none\"></li>");
            }
            ulBuilder.InnerHtml = stringBuilder.ToString();

            var divBuilder = new TagBuilder("div");
            divBuilder.AddCssClass(htmlHelper.ViewData.ModelState.IsValid ? HtmlHelper.ValidationSummaryValidCssClassName : HtmlHelper.ValidationSummaryCssClassName);
            divBuilder.InnerHtml = ulBuilder.ToString(TagRenderMode.Normal);
            if (formContextForClientValidation != null)
            {
                if (!htmlHelper.ViewContext.UnobtrusiveJavaScriptEnabled)
                {
                    divBuilder.GenerateId("validationSummary");
                    formContextForClientValidation.ValidationSummaryId = divBuilder.Attributes["id"];
                    formContextForClientValidation.ReplaceValidationSummary = false;
                }
            }
            return new HtmlString(divBuilder.ToString(TagRenderMode.Normal));
        }

        public static IHtmlString MyValidationMessageFor<TModel, TProperty>( this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> ex)
        {
            var htmlAttributes = new RouteValueDictionary();
            string validationMessage = null;
            var expression = ExpressionHelper.GetExpressionText(ex);
            var modelName = htmlHelper.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldName(expression);
            var formContext = htmlHelper.ViewContext.ClientValidationEnabled ? htmlHelper.ViewContext.FormContext : null;
            if (!htmlHelper.ViewData.ModelState.ContainsKey(modelName) && formContext == null)
            {
                return null;
            }

            var modelState = htmlHelper.ViewData.ModelState[modelName];
            var modelErrors = (modelState == null) ? null : modelState.Errors;
            var modelError = (((modelErrors == null) || (modelErrors.Count == 0))
                ? null
                : modelErrors.FirstOrDefault(m => !String.IsNullOrEmpty(m.ErrorMessage)) ?? modelErrors[0]);

            if (modelError == null && formContext == null)
            {
                return null;
            }

            var builder = new TagBuilder("span");
            builder.MergeAttributes(htmlAttributes);
            builder.AddCssClass((modelError != null) ? HtmlHelper.ValidationMessageCssClassName : HtmlHelper.ValidationMessageValidCssClassName);

            if (!string.IsNullOrEmpty(validationMessage))
            {
                builder.InnerHtml = validationMessage;
            }
            else if (modelError != null)
            {
                builder.InnerHtml = GetUserErrorMessageOrDefault(htmlHelper.ViewContext.HttpContext, modelError, modelState);
            }

            if (formContext != null)
            {
                bool replaceValidationMessageContents = String.IsNullOrEmpty(validationMessage);
                builder.MergeAttribute("data-valmsg-for", modelName);
                builder.MergeAttribute("data-valmsg-replace", replaceValidationMessageContents.ToString().ToLowerInvariant());
            }

            return new HtmlString(builder.ToString(TagRenderMode.Normal));
        }

        private static string GetUserErrorMessageOrDefault(HttpContextBase httpContext, ModelError error, ModelState modelState)
        {
            if (!String.IsNullOrEmpty(error.ErrorMessage))
            {
                return error.ErrorMessage;
            }
            if (modelState == null)
            {
                return null;
            }

            var attemptedValue = (modelState.Value != null) ? modelState.Value.AttemptedValue : null;
            return string.Format(CultureInfo.CurrentCulture, "Value '{0}' not valid for property", attemptedValue);
        }

        
    }
}