﻿using System;
using System.Data.SqlTypes;

namespace Vistech.Andon.Web.helpers
{
    public static class DbDateHelper
    {
        public static DateTime? ToNullIfTooEarlyForDb(this DateTime date)
        {
            return (date >= (DateTime)SqlDateTime.MinValue) ? date : (DateTime?)null;
        }
    }
}