﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Newtonsoft.Json;
using ProAlert.Andon.Service.Interfaces;
using ProAlert.Andon.Service.Models;
using ProAlert.Andon.Service.ViewModels;
using Vistech.Andon.Web.Tools;

namespace Vistech.Andon.Web.API
{
    //[System.Web.Mvc.Authorize(Roles = "Super")]
    [System.Web.Mvc.Authorize(Roles = "Operator, Admin, Super")]

    public class SummaryDetailsAPIController : ApiController
    {
        //#region < Members >

        //private IReadOnlyRepository _repo;

        //#endregion
        //public IReadOnlyRepository Repository
        //{
        //    get { return _repo; }
        //    set { _repo = value; }
        //}

        //[JsonConstructor]
        //public SummaryDetailsAPIController(IReadOnlyRepository repo)
        //{
        //    Repository = repo;
        //}

        public IEnumerable<ProductTimelineStatsVM> GetSummaryDetails()
        {
            var summaryStart = DateTime.UtcNow.AddHours(-8);
            var tlList = new List<ProductTimelineStatsVM>();
            if (HttpRuntime.Cache["OEESummaryStart"] != null)
            {
                // session is going to store UTC time  will have to convert the time to Client time when displaying in button.
                // if session set, then use
                summaryStart = (DateTime)HttpRuntime.Cache["OEESummaryStart"];
            }
            else
            {
                HttpRuntime.Cache.Insert("OEESummaryStart", summaryStart, null, DateTime.Now.AddHours(1), System.Web.Caching.Cache.NoSlidingExpiration);
                //Session["OEESummaryStart"] = summaryStart;
            }

            var yesterday = summaryStart;
            using (var context = new ProAlertContext())
            {
                //context.Configuration.ProxyCreationEnabled = false;
                using (var repo = new EntityFrameworkRepository<ProAlertContext>(context))
                {
                    var obj = new TlRpt();
                    tlList = obj.GetTls(repo.Get<WorkCenter>().ToList(), yesterday, DateTime.UtcNow);
                    foreach (var tl in tlList)
                    {
                        tl.CacheID = tl.TL.Id.ToString();
                        var oc = new OeeCache { Repo = repo };
                        oc.CacheSummaryTL(tl);
                    }
                }
            }


            return tlList;
        }
    }
}
